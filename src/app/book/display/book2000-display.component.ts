import { Component, OnInit, ViewChild } from '@angular/core';
import { Book } from 'src/app/shared/model/book';
import { Book2010DisplayDetailsComponent } from './book2010-display-details.component';
import { BookService } from 'src/app/shared/service/book.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AlertModalViewComponent } from 'src/app/shared/angular-material/alert-modal/alert-modal.component';
import { Author } from 'src/app/shared/model/author';
import { Publisher } from 'src/app/shared/model/publisher';
import { Author2010DisplayDetailsComponent } from 'src/app/author/display/author2010-display-details.component';
import { Publisher2010DisplayDetailsComponent } from 'src/app/publisher/display/publisher2010-display-details.component';

@Component({
  selector: 'app-book2000-display',
  templateUrl: './book2000-display.component.html',
  styleUrls: ['./book2000-display.component.scss']
})
export class Book2000DisplayComponent implements OnInit {

    @ViewChild(Book2010DisplayDetailsComponent)
    private book2010: Book2010DisplayDetailsComponent;
    @ViewChild(Author2010DisplayDetailsComponent)
    private author2010: Author2010DisplayDetailsComponent;
    @ViewChild(Publisher2010DisplayDetailsComponent)
    private publisher2010: Publisher2010DisplayDetailsComponent;

    public bookToDisplay: Book;
    public bookToUpdate: Book;
    public formDisable = true;

    public authorToDisplay: Author;
    public publisherToDisplay: Publisher;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private bookService: BookService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
        const id = this.activatedRoute.snapshot.params['id'];
        this.getBook(id);
    }

    getBook(id: number) {
        this.bookService
        .get(id)
        .subscribe(result => {
            this.bookToDisplay = result;
            this.authorToDisplay = result.author;
            this.publisherToDisplay = result.publisher;
            console.log(this.bookToDisplay);
        });
    }

    onClickEdit() {
        this.formDisable = false;
    }

    onClickReset() {
        this.formDisable = true;
    }

    onClickSave() {
        this.bookToUpdate = new Book();
        this.bookToUpdate = this.book2010.book2010FormValues();
        this.bookToUpdate.publishDate = this.bookToDisplay.publishDate;
        this.bookToUpdate.author = this.author2010.author2010FormValues();
        this.bookToUpdate.publisher = this.publisher2010.publisher2010FormValues();

        console.log(this.bookToUpdate);
        if (this.book2010.isFormValid()) {
            this.bookService
            .update(this.bookToUpdate)
            .subscribe((result: {success: boolean; contentList: Book[]; totalElements: number; }) => {
                if (result.success) {
                    // console.log('success');
                    this.bookToUpdate = new Book();
                    this.bookToUpdate = result.contentList[0];
                    this.formDisable = true;
                    this.openSuccessDialog('Edit Book', `Successfuly updated book`, 'success');
                    } else {
                        // console.log('fail');
                        // this.modalHeaderText = this.messageService.getValue('userWarning');
                        // this.modalBodyText = 'Update Failed';
                        // this.modalType = 'warning';
                        // this.alertModal.openDialog(this.modalHeaderText, this.modalBodyText, this.modalType);
                        this.openFailDialog('Edit Book', `Failed to update book.`, 'warning');
                    }
                });
        }
    }

    openSuccessDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.backToList();
        console.log('The dialog was closed');
        });
    }

    openFailDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
            const id = this.activatedRoute.snapshot.params['id'];
            this.getBook(id);
        });
    }

    backToList() {
        // this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/book/list');
    }

}
