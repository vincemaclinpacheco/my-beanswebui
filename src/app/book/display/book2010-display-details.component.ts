import { Component, OnInit, Input } from '@angular/core';
import { Book, Format } from 'src/app/shared/model/book';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-book2010-display-details',
  templateUrl: './book2010-display-details.component.html',
  styleUrls: ['./book2010-display-details.component.scss']
})
export class Book2010DisplayDetailsComponent implements OnInit {

    @Input()
    bookToDisplay: Book;

    @Input()
    formDisable: boolean;

    public Format = Format;
    public formatKeys = Object.keys(Format);

    public book2010Form: FormGroup;
    public book2010FormControl;

    public disabled = false;



    constructor(
        private fb: FormBuilder
    ) {
        this.createForm();
     }

    ngOnInit(): void {
    }

    ngOnChanges() {
        if (this.bookToDisplay) {
            this.book2010FormControl['id'].setValue(this.bookToDisplay.id);
            this.book2010FormControl['title'].setValue(this.bookToDisplay.title);
            this.book2010FormControl['format'].setValue(this.bookToDisplay.format);
            this.book2010FormControl['price'].setValue(this.bookToDisplay.price);
            // this.user2010FormControl['lastName'].setValue(this.userToDisplay.lastName);
            // this.user2010FormControl['role'].setValue(this.userToDisplay.role);
            // this.user2010FormControl['email'].setValue(this.userToDisplay.email);
            // const lastLoginDateMoment = MOMENT(new Date(this.userToDisplay.lastLoginDate));
            // this.user2010FormControl['lastLoginDate'].setValue(lastLoginDateMoment.format('MM/DD/YYYY HH:mm:ss'));
    
        }
    
        console.log(this.formDisable);
    }

    createForm() {
        this.book2010Form = this.fb.group({
            id: 0,
            title: '',
            format: '',
            price: 0
        });
        this.book2010FormControl = this.book2010Form.controls;
    }

    book2010FormValues() {
        this.markFormGroupTouched(this.book2010Form);
        return this.book2010Form.value;
    }
    
    isFormValid() {
        return this.book2010Form.valid;
    }
    
    markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
            control.markAsTouched();
                if (control.controls) {
                    this.markFormGroupTouched(control);
                }
            });
    }

}
