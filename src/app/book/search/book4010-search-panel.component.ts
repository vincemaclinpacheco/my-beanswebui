import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Format } from 'src/app/shared/model/book';

@Component({
  selector: 'app-book4010-search-panel',
  templateUrl: './book4010-search-panel.component.html',
  styleUrls: ['./book4010-search-panel.component.scss']
})
export class Book4010SearchPanelComponent implements OnInit {

    public book4010Form: FormGroup;
    public book4010FormControl;

    public Format = Format;
    public formatKeys = Object.keys(Format);
    
    constructor(
        private fb: FormBuilder
    ) {
        this.createForm();
     }

    ngOnInit(): void {
    }

    createForm() {
        this.book4010Form = this.fb.group({
            id: 0,
            title: '',
            format: ''
        });
        this.book4010FormControl = this.book4010Form.controls;
    }

    book4010FormValues() {
        return this.book4010Form.value;
    }

    onReset() {
        this.createForm();
    }

}
