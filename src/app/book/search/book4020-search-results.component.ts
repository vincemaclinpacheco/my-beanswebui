import { Component, OnInit, Input, NgZone } from '@angular/core';
import { Book } from 'src/app/shared/model/book';
import { GridOptions } from 'ag-grid-community';
import { BookService } from 'src/app/shared/service/book.service';
import { Router } from '@angular/router';
import { DateRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/date-renderer.component';

@Component({
  selector: 'app-book4020-search-results',
  templateUrl: './book4020-search-results.component.html',
  styleUrls: ['./book4020-search-results.component.scss']
})
export class Book4020SearchResultsComponent implements OnInit {

    @Input()
    searchedBookList: Book[] = [];

    @Input()
    totalElements: number;

    public gridOptions: GridOptions;
    public gridApi;
    public gridColumnApi;

    public selectedBook: Book;

    constructor(
        private bookService: BookService,
        private router: Router,
        private ngZone: NgZone
    ) {
        this.gridOptions = { 
            columnDefs: [
                { headerName: 'Title', field: 'title', width: 300, sortable: true },
                { headerName: 'Author', width: 250, colId: 'authorFullName', sortable: true, valueGetter: function nameConcat(params) {
                    return params.data.author.firstName + ' ' + params.data.author.lastName;
                }},
                { headerName: 'Publisher', field: 'publisher.name', sortable: true, width: 250 },
                { headerName: 'Published Date', field: 'publishDate', width: 130, cellRenderer: 'dateRenderer' },
                { headerName: 'Format', field: 'format', width: 130 },
                { headerName: 'Price', field: 'price', width: 130, sortable: true },
    
             ],
             enableColResize: true,
             rowSelection: 'single',
             frameworkComponents : {
                dateRenderer: DateRendererComponent
            }
        };
     }

    ngOnInit(): void {
    }

    onSelectBook() {
        const selectedNode = this.gridApi.getSelectedNodes();
        const selectedData = selectedNode.map( node => node.data );
        this.selectedBook = selectedData[0];
        this.changeRoute(this.selectedBook.id);
   
    }

    changeRoute(id: number) {
        // this.router.navigate(['user/display/' + id]);
        this.ngZone.run(() => this.router.navigate(['book/display/' + id])).then();
    }

    refreshGrid() {
        this.gridApi.refreshCells();
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

}
