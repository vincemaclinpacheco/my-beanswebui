import { Component, OnInit, ViewChild } from '@angular/core';
import { Book4010SearchPanelComponent } from './book4010-search-panel.component';
import { QueryParam, Operator } from 'src/app/shared/model/query';
import { Book } from 'src/app/shared/model/book';
import { BookService } from 'src/app/shared/service/book.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book4000-search',
  templateUrl: './book4000-search.component.html',
  styleUrls: ['./book4000-search.component.scss']
})
export class Book4000SearchComponent implements OnInit {

    @ViewChild(Book4010SearchPanelComponent)
    private book4010Form: Book4010SearchPanelComponent;

    public queryParams: QueryParam[] = [];
    public searchedBookList: Book[] = [];
    public pageSize = 25;
    public totalElements: number;

    public bookForm: any;

    constructor(
        private bookService: BookService,
        private router: Router
    ) { }

    ngOnInit(): void {
    }

    onClickReset() {
        this.book4010Form.onReset();
    }

    onClickSearch() {
        this.queryParams = [];
        this.searchedBookList = [];

        this.bookForm = this.book4010Form.book4010FormValues();
        /* Book Form Parameter */
        if (this.bookForm.title !== '') {
            let titleParam: QueryParam;
            this.bookForm.title = this.bookForm.title.trim();
            this.bookForm.title = `*${this.bookForm.title}*`
  
            titleParam = new QueryParam('title', Operator.LIKE, this.bookForm.title);

            this.queryParams.push(titleParam);
        }

        if (this.bookForm.format !== '') {
            const formatParam = new QueryParam('format', Operator.EQIC, this.bookForm.format);
            this.queryParams.push(formatParam);
        }

        // if (this.bookForm.lastName !== '') {
        //     const lastNameParam = new QueryParam('lastName', Operator.LIKE, this.bookForm.lastName);
        //     this.queryParams.push(lastNameParam);
        // }

        // if (this.bookForm.role !== '') {
        //     const roleParam = new QueryParam('role', Operator.EQIC, this.bookForm.role);
        //     this.queryParams.push(roleParam);
        // }

        // if (this.bookForm.email !== '') {
        //     const emailParam = new QueryParam('email', Operator.EQIC, this.bookForm.email);
        //     this.queryParams.push(emailParam);
        // }

        
        // if (this.bookForm.lastLoginStartDate !== '') {
        //     const saleStartDateParam = new QueryParam('lastLoginDate', Operator.GTE, this.bookForm.lastLoginStartDate);
        //     this.queryParams.push(saleStartDateParam);
        // }

        // if (this.bookForm.lastLoginEndDate !== '') {
        //     const saleEndDateParam = new QueryParam('lastLoginDate', Operator.LTE, this.bookForm.lastLoginEndDate);
        //     this.queryParams.push(saleEndDateParam);
        // }

        this.searchBook(0, this.pageSize);

    }

    searchBook(page: number, pageSize: number) {
        this.bookService
        .search(this.queryParams, page, pageSize)
        .subscribe((result: any) => {
            this.searchedBookList = result.contentList;
            this.totalElements = result.totalElements;
            console.log(this.searchedBookList);
        });
    }

    checkBookResultLength() {
        if (this.searchedBookList.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    onClickBack() {
        this.searchedBookList = [];
    }

}
