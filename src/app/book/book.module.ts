import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { AgGridCustomModule } from '../shared/ag-grid-custom/ag-grid-custom.module';
import { AuthorModule } from '../author/author.module';
import { PublisherModule } from '../publisher/publisher.module';
import { BookRoutingModule } from './book-routing.module';
import { Book1000ListComponent } from './list/book1000-list.component';
import { BookComponent } from './book.component';
import { Book1010ListTableComponent } from './list/book1010-list-table.component';
import { Book2000DisplayComponent } from './display/book2000-display.component';
import { Book2010DisplayDetailsComponent } from './display/book2010-display-details.component';
import { Book4000SearchComponent } from './search/book4000-search.component';
import { Book4010SearchPanelComponent } from './search/book4010-search-panel.component';
import { Book4020SearchResultsComponent } from './search/book4020-search-results.component';
import { Book3000FormComponent } from './form/book3000-form.component';
import { Book3010FormPanelComponent } from './form/book3010-form-panel.component';

@NgModule({
  declarations: [Book1000ListComponent, BookComponent, Book1010ListTableComponent, 
                    Book2000DisplayComponent, Book2010DisplayDetailsComponent, Book4000SearchComponent, Book4010SearchPanelComponent, Book4020SearchResultsComponent, Book3000FormComponent, Book3010FormPanelComponent],
  imports: [
    CommonModule,
    BookRoutingModule,
    AngularMaterialModule,
    AgGridCustomModule,
    SharedModule,
    AuthorModule,
    PublisherModule
  ]
})
export class BookModule { }
