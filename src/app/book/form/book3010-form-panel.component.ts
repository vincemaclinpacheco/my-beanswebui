import { Component, OnInit } from '@angular/core';
import { Book, Format } from 'src/app/shared/model/book';
import { BookService } from 'src/app/shared/service/book.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-book3010-form-panel',
  templateUrl: './book3010-form-panel.component.html',
  styleUrls: ['./book3010-form-panel.component.scss']
})
export class Book3010FormPanelComponent implements OnInit {

    public book3010Form: FormGroup;
    public book3010FormControl;

    public Format = Format;
    public formatKeys = Object.keys(Format);

    public hide = false;

    public errorMessage = 'Required Field';

    public formDisable: false;

    public newBook = new Book();

    public publishDateLabel = 'Publish Date';
    public resetDate = false;
    public required = true;
    public saveDateAttempt = false;
    public publishDateDisplay: string;

    constructor(
        private fb: FormBuilder,
        private bookService: BookService
    ) {
        this.createForm();
     }

    ngOnInit(): void {
    }

    createForm() {
        this.book3010Form = this.fb.group({
            title: ['', [Validators.required]],
            format: ['', [Validators.required]],
            price: [0, [Validators.required]],
            publishDate: ['', [Validators.required]]
        });
        this.book3010FormControl = this.book3010Form.controls;
    }

    book3010FormValues() {
        this.markFormGroupTouched(this.book3010Form);
        return this.book3010Form.value;
    }

    isFormValid() {
        return this.book3010Form.valid;
    }

    markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
        control.markAsTouched();
        if (control.controls) {
            this.markFormGroupTouched(control);
        }
        });
    }

    onReset() {
        this.createForm();
        // console.log(this.user3010Form.hasError('notMatch'));
    }

    onSelectPublishDate(dateSelected: string) {
        this.book3010FormControl['publishDate'].setValue(dateSelected);
    }

    onAttemptSaveDate(status: boolean) {
        if (this.saveDateAttempt === true) {
            this.saveDateAttempt = status;
        }
    }

    onResetPublishDate(status: boolean) {
        if (this.resetDate === true) {
            this.resetDate = status;
        }
    }

}
