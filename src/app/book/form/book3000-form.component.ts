import { Component, OnInit, ViewChild } from '@angular/core';
import { Book3010FormPanelComponent } from './book3010-form-panel.component';
import { Book } from 'src/app/shared/model/book';
import { BookService } from 'src/app/shared/service/book.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AlertModalViewComponent } from 'src/app/shared/angular-material/alert-modal/alert-modal.component';
import { Author3010FormPanelComponent } from 'src/app/author/form/author3010-form-panel.component';
import { Publisher3010FormPanelComponent } from 'src/app/publisher/form/publisher3010-form-panel.component';

@Component({
  selector: 'app-book3000-form',
  templateUrl: './book3000-form.component.html',
  styleUrls: ['./book3000-form.component.scss']
})
export class Book3000FormComponent implements OnInit {

    @ViewChild(Book3010FormPanelComponent)
    private book3010: Book3010FormPanelComponent;
    @ViewChild(Author3010FormPanelComponent)
    private author3010: Author3010FormPanelComponent;
    @ViewChild(Publisher3010FormPanelComponent)
    private publisher3010: Publisher3010FormPanelComponent;

    public newBook: Book;

    constructor(
        private router: Router,
        private bookService: BookService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
    }

    onClickReset() {
        this.book3010.onReset();
    }

    onClickAdd() {
        this.newBook = new Book();
        this.newBook = this.book3010.book3010FormValues();
        this.newBook.author = this.author3010.author3010FormValues();
        this.newBook.publisher = this.publisher3010.publisher3010FormValues();
        this.book3010.saveDateAttempt = true;
        console.log(this.newBook);
            if (this.book3010.isFormValid() && this.author3010.isFormValid() && this.publisher3010.isFormValid()) {
                console.log('valid');
                this.bookService.create(this.newBook)
                    .subscribe((result: {success: boolean; contentList: Book[]; totalElements: number}) => {
                        if (result.success) {
                            console.log('success');
                            this.openSuccessDialog('Create Book', `Successfuly created book`, 'success');
                        } else {
                                console.log('fail');
                            this.openFailDialog('Create Book', `Failed to create new book.`, 'warning');
                        }
                    });
            } else {

            }
    }

    openSuccessDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.backToList();
        console.log('The dialog was closed');
        });
    }

    openFailDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.onClickReset();
        console.log('The dialog was closed');
        });
    }

    backToList() {
        // this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/book/list');
    }

}
