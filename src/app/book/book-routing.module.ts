import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './book.component';
import { Book1000ListComponent } from './list/book1000-list.component';
import { Book2000DisplayComponent } from './display/book2000-display.component';
import { Book3000FormComponent } from './form/book3000-form.component';
import { Book4000SearchComponent } from './search/book4000-search.component';


const BOOK_ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: '',
        component: BookComponent,
        children: [
            {
                path: 'list',
                component: Book1000ListComponent
            },
            {
                path:'display/:id',
                component: Book2000DisplayComponent
            },
            {
                path:'new',
                component: Book3000FormComponent
            },
            {
                path:'search',
                component: Book4000SearchComponent
            },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(BOOK_ROUTES)],
  exports: [RouterModule]
})
export class BookRoutingModule { }
