import { Component, OnInit, NgZone } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { Book } from 'src/app/shared/model/book';
import { BookService } from 'src/app/shared/service/book.service';
import { Router } from '@angular/router';
import { DateRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/date-renderer.component';

@Component({
  selector: 'app-book1010-list-table',
  templateUrl: './book1010-list-table.component.html',
  styleUrls: ['./book1010-list-table.component.scss']
})
export class Book1010ListTableComponent implements OnInit {

    public gridOptions: GridOptions;
    public gridApi;
    public gridColumnApi;

    public totalElements: number;
    public bookList: Book[] = [];
    public selectedBook: Book;

    // MatPaginator
    public pageSize = 10;
    public pageSizeOptions: number[] = [10, 20];

    constructor(
        private bookService: BookService,
        private router: Router,
        private ngZone: NgZone
    ) {
        this.gridOptions = { 
            columnDefs: [
                { headerName: 'Title', field: 'title', width: 300, sortable: true },
                { headerName: 'Author', width: 250, colId: 'authorFullName', sortable: true, valueGetter: function nameConcat(params) {
                    return params.data.author.firstName + ' ' + params.data.author.lastName;
                }},
                { headerName: 'Publisher', field: 'publisher.name', sortable: true, width: 250 },
                { headerName: 'Published Date', field: 'publishDate', width: 130, cellRenderer: 'dateRenderer' },
                { headerName: 'Format', field: 'format', width: 130 },
                { headerName: 'Price', field: 'price', width: 130, sortable: true },
    
             ],
             enableColResize: true,
             rowSelection: 'single',
             frameworkComponents : {
                dateRenderer: DateRendererComponent
            }
        };
     }

    ngOnInit(): void {
        this.listBooks(0, 10);
    }

    listBooks(pageNo: number, pageSize: number) {
        this.bookService
            .list(pageNo, pageSize)
            .subscribe((result: {success: boolean; contentList: Book[]; totalElements: number; }) => {
                this.bookList = result.contentList;
                this.totalElements = result.totalElements;
                console.log(this.bookList);
            });
    }

    onSelectBook() {
        const selectedNode = this.gridApi.getSelectedNodes();
        const selectedData = selectedNode.map( node => node.data );
        this.selectedBook = selectedData[0];
        console.log(this.selectedBook);
        this.changeRoute(this.selectedBook.id);
   
    }

    changeRoute(id: number) {
        // this.router.navigate(['user/display/' + id]);
        this.ngZone.run(() => this.router.navigate(['book/display/' + id])).then();
    }

    onPageSelect(e: any) {
        if (this.bookList.length > 0) {
            this.listBooks(e.pageIndex, e.pageSize);
        }
    }

    refreshGrid() {
    this.gridApi.refreshCells();
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

}
