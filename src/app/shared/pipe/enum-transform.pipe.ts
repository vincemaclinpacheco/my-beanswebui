import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'enumTransform'
})

export class EnumTransformPipe implements PipeTransform {
    public retainUpperAr = ['lbc', 'dhl'];
    public retainLowerAr = ['and', 'to'];

    transform(value: string): string {
        if (value.search('_')) {
            let transformedValue = '';
            const transformToSpace = value.split('_').join(' ');
            const transformToSpaceLowerCase = transformToSpace.toLowerCase();
            const wordAr = transformToSpaceLowerCase.split(' ');
            if (wordAr.length === 1) {
                if (this.retainUpperAr.includes(wordAr[0])) {
                    return value;
                } else {
                    const transformToLowerCase = value.toLowerCase();
                    const firstLetter = transformToLowerCase[0].toUpperCase();
                    const rest = transformToLowerCase.slice(1);
                    transformedValue = firstLetter + rest;
                    return transformedValue;
                }
            } else {
                for (const word of wordAr) {
                    if (this.retainLowerAr.includes(word)) {
                        transformedValue += word + ' ';
                    } else {
                        const firstLetter = word[0].toUpperCase();
                        const rest = word.slice(1);
                        const transformedWord = firstLetter + rest;
                        transformedValue += transformedWord + ' ';
                    }
                }
                return transformedValue;
            }
        } else {
            if (this.retainUpperAr.includes(value)) {
                return value;
            } else {
                let transformedValue = '';
                const transformToLowerCase = value.toLowerCase();
                const firstLetter = transformToLowerCase[0].toUpperCase();
                const rest = transformToLowerCase.slice(1);
                transformedValue = firstLetter + rest;
                return transformedValue;
            }
        }
    }
}
