import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';

import { DateRendererComponent } from './renderer/date-renderer.component';
import { DatetimeRendererComponent } from './renderer/datetime-renderer.component';

@NgModule({
    declarations: [
        DateRendererComponent,
        DatetimeRendererComponent
    ],
    imports: [
        CommonModule,
        AgGridModule.withComponents([DateRendererComponent, DatetimeRendererComponent])
    ],
    exports: [
        AgGridModule,
        DateRendererComponent,
        DatetimeRendererComponent
    ],
    entryComponents: [
    ]
})
export class AgGridCustomModule { }