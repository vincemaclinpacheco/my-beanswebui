import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MOMENT } from '../../angular-material/angular-material.module';

@Component({
    selector: 'app-date-renderer',
    template: `
        {{ dateDisplay }}
    `
})
export class DateRendererComponent implements ICellRendererAngularComp {
    dateDisplay: string;

    constructor() { }

    agInit(params: any) {
        if (params.value) {
            const dateValue = MOMENT(params.value);
            this.dateDisplay = dateValue.format('MM/DD/YYYY');
        } else {
            this.dateDisplay = '';
        }
    }

    refresh(): boolean {
        return false;
    }

}
