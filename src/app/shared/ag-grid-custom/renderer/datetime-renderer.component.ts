import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MOMENT } from '../../angular-material/angular-material.module';

@Component({
    selector: 'app-datetime-renderer',
    template: `
        {{ dateTimeDisplay }}
    `
})
export class DatetimeRendererComponent implements ICellRendererAngularComp {
    dateTimeDisplay: string;

    constructor() { }

    agInit(params: any) {
        if (params.value) {
            const dateValue = MOMENT(params.value);
            this.dateTimeDisplay = dateValue.format('L hh:mm A');
        } else {
            this.dateTimeDisplay = '';
        }

    }

    refresh(): boolean {
        return false;
    }
}
