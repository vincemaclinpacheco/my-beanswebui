import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { LocalStorageService, SessionStorageService, SessionStorage } from 'ngx-webstorage';
import { AuthService } from '../service/auth.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AlertModalViewComponent } from '../angular-material/alert-modal/alert-modal.component';

@Injectable({
    providedIn: 'root'
})
export class RoleGuardService implements CanActivate {

    public allow: boolean;

    constructor(
        public router: Router,
        public authService: AuthService,
        public dialog: MatDialog
    ) { }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        const expectedRole = route.data.expectedRole;
        const user = this.authService.getUser();
        if (user.role !== expectedRole) {
            this.openAlertDialog('Unauthorized', 'You are not authorized to view this page.', 'warning');
            return false;
        }
        return true;

    }

    openAlertDialog(headerText: string, bodyText: string, type: string) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.width = '500px';
        dialogConfig.height = '200px';
        dialogConfig.closeOnNavigation = false;
        dialogConfig.data = {
            header: headerText,
            body: bodyText,
            alertType: type
        };

        const dialogRef = this.dialog.open(AlertModalViewComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
                // console.log('The dialog was closed');
        });
    }

}