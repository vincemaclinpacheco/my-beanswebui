import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LocalStorageService, SessionStorageService, SessionStorage } from 'ngx-webstorage';
import { AuthService } from '../service/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    @SessionStorage('beansData')
    beansDataAttribute;

    constructor(
        public router: Router,
        public authService: AuthService
    ) { }

    canActivate(): boolean {
        const isAuthenticated = this.authService.isAuthenticated();
        if (!isAuthenticated) {
                this.router.navigate(['auth']);
            }
        return isAuthenticated;
    }

}