import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

    public baseUrl: string;

  constructor() { }

    getAppBaseUrl() {
        return `${environment.baseUrl}`;
    }
}
