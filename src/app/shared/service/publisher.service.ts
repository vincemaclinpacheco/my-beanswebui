import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {plainToClass} from "class-transformer";
import { QueryParam } from '../model/query';
import { Book } from '../model/book';
import { Publisher } from '../model/Publisher';

const RESOURCE_PUBLISHER = '/publishers';
const RESOURCE_LIST = '/list';
const RESOURCE_DTO = '/dto';
const RESOURCE_SEARCH = '/query/results';
const RESOURCE_UPDATE = '/update';

@Injectable({
  providedIn: 'root'
})
export class PublisherService {

    getResourceUrl(): string {
        return `/${this.utilService.getAppBaseUrl()}${RESOURCE_PUBLISHER}`;
    }

    constructor(
        private utilService: UtilService,
        private httpClient: HttpClient
    ) { }


    public get(id: number): Observable<any> {
        return this.httpClient
                .get(`${this.getResourceUrl()}/${id}`)
                .pipe(map((result: {success: boolean; contentList: Publisher[]; totalElements: number}) => {
                    return plainToClass(Publisher, result.contentList[0]) ;
                }), catchError(this.handleError));
    }

    public create(newPublisher: Publisher) {
        return this.httpClient
            .post(`${this.getResourceUrl()}`, newPublisher)
            .pipe(map((result: { success: boolean; contentList: Publisher[]; totalElements: number }) => {
                return {
                    success: result.success,
                    contentList: plainToClass(Publisher, result.contentList),
                    totalElements: result.totalElements
                };
            }), catchError(this.handleError));
    }

    public update(publisher: Publisher): Observable<any> {
        return this.httpClient
            .put(`${this.getResourceUrl()}/${publisher.id}`, publisher)
            .pipe(
                map((result:
                    {
                        success: boolean;
                        contentList: Publisher[];
                        totalElements: number
                    }) => {
                    return {
                        success: result.success,
                        contentList: plainToClass(Publisher, result.contentList),
                        totalElements: result.totalElements
                    };
                }), catchError(this.handleError));
    }

    public list(page: number, pageSize: number): Observable<any> {
        const htppParams = new HttpParams()
                            .set('page', page.toString())
                            .set('pageSize', pageSize.toString());
        return this.httpClient
            .get(`${this.getResourceUrl()}${RESOURCE_LIST}`, { params: htppParams })
            .pipe(map((result: {success: boolean; contentList: Publisher[]; totalElements: number}) => {
                return {
                    success      : result.success,
                    contentList  : plainToClass(Publisher, result.contentList),
                    totalElements : result.totalElements
                };
            }), catchError(this.handleError));
    }

    public search(qryParams: QueryParam[], page: number, pageSize: number) {
        const httpParams = new HttpParams()
            .set('page', page.toString())
            .set('pageSize', pageSize.toString());
        return this.httpClient
            .post(`${this.getResourceUrl()}${RESOURCE_SEARCH}`, qryParams, { params: httpParams })
            .pipe(map((result: { success: boolean; contentList: Publisher[]; totalElements: number }) => {
                return {
                    success: result.success,
                    contentList: plainToClass(Publisher, result.contentList),
                    totalElements: result.totalElements
                };
            }), catchError(this.handleError));
    }


    private handleError(error: any) {
        return throwError(error);
    }
}
