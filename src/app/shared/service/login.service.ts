import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UtilService } from './util.service';


const RESOURCE_AUTH = '/auth';
const RESOURCE_LOGIN = '/login';
const RESOURCE_CHANGE_PASSWORD = '/change_password';

@Injectable({
  providedIn: 'root'
})


export class LoginService {

    public userToLogin = new User({
        userId: '',
        password: ''
    });

    getResourceUrl(): string {
        return `/${this.utilService.getAppBaseUrl()}${RESOURCE_AUTH}`;
    }

  constructor(
    private httpClient: HttpClient,
    private utilService: UtilService
  ) { }


  public loginUser(user: User) {
    // console.log(`loginUser: ${this.getResourceUrl()}${RESOURCE_LOGIN}`);
    return this.httpClient
        .post(`${this.getResourceUrl()}${RESOURCE_LOGIN}`, user, {withCredentials: true})
        .pipe(
        map((result:
            {
                success: boolean;
                contentList: any;
                totalElements: number;
                errorMessage: any,
            }) => {
            return {
                success: result.success,
                contentList: result.contentList,
                totalElements: result.totalElements,
                errorMessage: result.errorMessage
            };
        }
    ), catchError(this.handleError));
}

    updatePassword(user: User) {
        return this.httpClient
            .post(`${this.getResourceUrl()}${RESOURCE_LOGIN}${RESOURCE_CHANGE_PASSWORD}`, user)
            .pipe(
            map((result:
                {
                    success: boolean;
                    contentList: any;
                    totalElements: number
                }) => {
                return {
                    success: result.success,
                    contentList: result.contentList,
                    totalElements: result.totalElements
                };
            }
        ), catchError(this.handleError));
    }

clearUserToLogin() {
    this.userToLogin = new User();
}


private handleError(error: any) {
    return throwError(error);
}

}
