import { Injectable } from '@angular/core';
import { LocalStorageService, SessionStorageService, SessionStorage } from 'ngx-webstorage';
import * as crypto from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    public CryptoJS = require('crypto-js');
    @SessionStorage('beansData')
    beansDataAttribute;

    constructor(
        private sessionStorageService: SessionStorageService
    ) { }

    public getSession() {
        if (this.beansDataAttribute) {
            const encyptedSession = this.beansDataAttribute;
            const bytes = this.CryptoJS.AES.decrypt(encyptedSession, 'b3@ns');
            const decryptedData = JSON.parse(bytes.toString(this.CryptoJS.enc.Utf8));
            return decryptedData[0];
        } else {
            return null;
        }
    }

    public isAuthenticated(): boolean {
        if (this.getSession() !== null) {
            return true;
        } else {
            return false;
        }
    }

    public getUser() {
        return this.getSession().user;
    }
}
