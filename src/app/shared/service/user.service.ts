import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { User } from '../model/user';
import { map, catchError } from 'rxjs/operators';
import {plainToClass} from "class-transformer";
import { QueryParam } from '../model/query';

const RESOURCE_USER = '/users';
const RESOURCE_LIST = '/list';
const RESOURCE_SEARCH = '/query/results';
const RESOURCE_UPDATE = '/update';
const RESOURCE_UNLOCK = '/unlock';
const RESOURCE_SEQNO = '/seqno';
const RESOURCE_RESET_PASSWORD = 'reset_password';
const RESOURCE_MODULE = '/module';
const RESOURCE_MODULE_ACCESS = '/module/access';

@Injectable({
  providedIn: 'root'
})
export class UserService {

getResourceUrl(): string {
    return `/${this.utilService.getAppBaseUrl()}${RESOURCE_USER}`;
}

  constructor(
    private utilService: UtilService,
    private httpClient: HttpClient
  ) { }


  public getUser(id: number): Observable<any> {
    return this.httpClient
            .get(`${this.getResourceUrl()}/${id}`)
            .pipe(map((result: {success: boolean; contentList: User[]; totalElements: number}) => {
                return plainToClass(User, result.contentList[0]) ;
            }), catchError(this.handleError));
}

public createUser(newUser: User) {
    return this.httpClient
        .post(`${this.getResourceUrl()}`, newUser)
        .pipe(map((result: { success: boolean; contentList: User[]; totalElements: number }) => {
            return {
                success: result.success,
                contentList: plainToClass(User, result.contentList),
                totalElements: result.totalElements
            };
        }), catchError(this.handleError));
}

public updateUser(user: User): Observable<any> {
    return this.httpClient
        .put(`${this.getResourceUrl()}/${user.id}`, user)
        .pipe(
            map((result:
                {
                    success: boolean;
                    contentList: User[];
                    totalElements: number
                }) => {
                return {
                    success: result.success,
                    contentList: plainToClass(User, result.contentList),
                    totalElements: result.totalElements
                };
            }), catchError(this.handleError));
}

public listUsers(page: number, pageSize: number): Observable<any> {
    const htppParams = new HttpParams()
                        .set('page', page.toString())
                        .set('pageSize', pageSize.toString());
    return this.httpClient
        .get(`${this.getResourceUrl()}${RESOURCE_LIST}`, { params: htppParams })
        .pipe(map((result: {success: boolean; contentList: User[]; totalElements: number}) => {
            return {
                success      : result.success,
                contentList  : plainToClass(User, result.contentList),
                totalElements : result.totalElements
            };
        }), catchError(this.handleError));
}

public searchUser(userQryParams: QueryParam[], page: number, pageSize: number) {
    const httpParams = new HttpParams()
        .set('page', page.toString())
        .set('pageSize', pageSize.toString());
    return this.httpClient
        .post(`${this.getResourceUrl()}${RESOURCE_SEARCH}`, userQryParams, { params: httpParams })
        .pipe(map((result: { success: boolean; contentList: User[]; totalElements: number }) => {
            return {
                success: result.success,
                contentList: plainToClass(User, result.contentList),
                totalElements: result.totalElements
            };
        }), catchError(this.handleError));
}


private handleError(error: any) {
    return throwError(error);
}
}
