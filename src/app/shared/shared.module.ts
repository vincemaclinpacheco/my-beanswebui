import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DisableControlDirective } from './directive/disable-control.directive';
import { EnumTransformPipe } from './pipe/enum-transform.pipe';

@NgModule({
  declarations: [
    DisableControlDirective,
    EnumTransformPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    DisableControlDirective,
    EnumTransformPipe
]
})
export class SharedModule { }
