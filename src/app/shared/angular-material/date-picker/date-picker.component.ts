import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewEncapsulation, SimpleChanges } from '@angular/core';
import { FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MOMENT } from '../angular-material.module';
import { MAT_DATE_FORMATS } from '@angular/material/core';

const DATE_FORMAT = {
    parse: {
        dateInput: 'L'
    },
    display: {
        dateInput: 'L',
        monthYearLabel: 'MMM YYYY'
    }
};

@Component({
    selector: 'app-date-picker',
    templateUrl: './date-picker.component.html',
    styleUrls: ['./date-picker.component.scss'],
    providers: [
        { provide: MAT_DATE_FORMATS, useValue: DATE_FORMAT }
    ]
})
export class DatePickerComponent implements OnInit, OnChanges {

    @Input()
    initialDate: boolean;

    @Input()
    displayDate;

    @Input()
    saveDateAttempt: boolean;

    @Input()
    resetDate: boolean;

    @Input()
    fieldName: string;

    @Input()
    minDate: Date;

    @Input()
    maxDate: Date;

    @Input()
    required: boolean;

    @Input()
    disabled: boolean;

    @Input()
    dateFormat = 'YYYY-MM-DD';

    @Output()
    dateSelection = new EventEmitter<string>();

    @Output()
    dateResetEvent = new EventEmitter<boolean>();

    @Output()
    dateSaveAttemptEvent = new EventEmitter<boolean>();

    public dateForm = new FormControl('');
    public dateFieldName = '';
    public minimumDate: Date;
    public maximumDate: Date;
    public isRequired: boolean;
    public dateError = 'Date format is MM/DD/YYYY';

    constructor() { }

    ngOnInit() {
        if (this.fieldName) {
            this.dateFieldName = this.fieldName;
        }

        if (this.initialDate) {
            this.setInitialDate();
        }

        if (this.minDate) {
            this.minimumDate = this.minDate;
        } else {
            this.minimumDate = new Date('01/01/2000');
        }

        if (this.maxDate) {
            this.maximumDate = this.maxDate;
        }

        if (this.required) {
            this.isRequired = this.required;
            this.dateForm.setValidators([Validators.required]);
        } else {
            this.isRequired = false;
        }

    }

    ngOnChanges() {
        // setTimeout is used to avoid ngDoCheck error
        setTimeout(() => {
            if (this.resetDate === true) {
                if (this.initialDate) {
                    this.setInitialDate();
                } else {
                    if (this.disabled) {
                        this.dateForm = new FormControl({ value: '', disabled: true });
                    } else {
                        this.dateForm = new FormControl('');
                    }


                    if (this.isRequired) {
                        this.dateForm.setValidators([Validators.required]);
                    }
                }
                this.dateResetEvent.emit(false);
            }

            if (this.saveDateAttempt === true) {
                this.datePickerFormTouched();
                this.dateSaveAttemptEvent.emit(false);
            }
        }, 0);

        if (this.displayDate) {
            const displayDate = MOMENT(new Date(this.displayDate));
            this.dateForm.setValue(displayDate);
            this.dateSelection.emit(this.dateForm.value.format(this.dateFormat));
        }

        if (this.minDate) {
            this.minimumDate = this.minDate;
        }
    }

    setInitialDate() {
        const newDate = MOMENT(new Date());
        this.dateForm.setValue(newDate);
        this.dateSelection.emit(this.dateForm.value.format(this.dateFormat));
    }

    onSelectDate(dateSelected: any) {
        if (dateSelected.value) {
            const dateMoment = dateSelected.value.format(this.dateFormat);
            if (dateMoment < MOMENT(this.minimumDate).format(this.dateFormat)) {
                this.dateSelection.emit(null);
            } else {
                this.dateSelection.emit(dateMoment);
            }
        } else {
            this.dateSelection.emit(null);
        }
    }

    datePickerFormTouched() {
        this.dateForm.markAsTouched();
    }

}
