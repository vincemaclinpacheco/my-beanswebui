import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from 'saturn-datepicker';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import { DateRange } from '../../model/form';

const DATE_FORMAT = {
    parse: {
        dateInput: 'L'
    },
    display: {
        dateInput: 'L',
        monthYearLabel: 'MMM YYYY'
    }
};

@Component({
    selector: 'app-daterange-picker',
    templateUrl: './daterange-picker.component.html',
    styleUrls: ['./daterange-picker.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: DATE_FORMAT }
    ]
})
export class DaterangePickerComponent implements OnInit, OnChanges {

    @Input()
    fieldName: string;

    @Input()
    resetDateRange: boolean;

    @Output()
    dateRangeSelection = new EventEmitter<DateRange>();

    @Output()
    dateRangeResetEvent = new EventEmitter<boolean>();

    @Input()
    dateFormat = 'YYYY-MM-DD';

    public dateRangeForm = new FormControl('');
    public dateRangeFieldName = '';

    constructor() { }

    ngOnInit() {
        if (this.fieldName) {
            this.dateRangeFieldName = this.fieldName;
        }
    }

    ngOnChanges() {
        setTimeout(() => {
            if (this.resetDateRange) {
                this.dateRangeForm = new FormControl('');
                this.dateRangeResetEvent.emit(false);
            }
        }, 0);
    }

    onSelectDateRange(dateRangeSelected: any) {
        if (dateRangeSelected.value) {
            const dateValue = dateRangeSelected.value;
            const dateRangeValue = new DateRange();
            dateRangeValue.begin = dateValue.begin.format(this.dateFormat);
            dateRangeValue.end = dateValue.end.format(this.dateFormat);
            this.dateRangeSelection.emit(dateRangeValue);
        } else {
            this.dateRangeSelection.emit(null);
        }
    }

}
