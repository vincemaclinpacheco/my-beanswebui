import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { FormControl, Validators } from '@angular/forms';
import { MOMENT } from '../angular-material.module';

const DATE_TIME_FORMAT = {
    parse: {
        // dateInput: 'L hh:mm A'
    },
    display: {
        dateInput: 'L hh:mm A',
        monthYearLabel: 'MMM YYYY'
    }
};

@Component({
    selector: 'app-datetime-picker',
    templateUrl: './datetime-picker.component.html',
    styleUrls: ['./datetime-picker.component.scss'],
    providers: [
        { provide: MAT_DATE_FORMATS, useValue: DATE_TIME_FORMAT }
    ]
})
export class DatetimePickerComponent implements OnInit, OnChanges {

    @Input()
    initialDateTime: boolean;

    @Input()
    displayDateTime: string;

    @Input()
    saveDateTimeAttempt: boolean;

    @Input()
    resetDateTime: boolean;

    @Input()
    fieldName: string;

    @Input()
    minDate: Date;

    @Input()
    required: boolean;

    @Input()
    disabled: boolean;

    @Output()
    dateTimeSelection = new EventEmitter<string>();

    @Output()
    dateTimeResetEvent = new EventEmitter<boolean>();

    @Output()
    dateTimeSaveAttemptEvent = new EventEmitter<boolean>();

    public dateTimeForm = new FormControl('');
    public dateTimeFieldName = '';
    public minimumDate: Date;
    public isRequired: boolean;
    public dateTimeError = 'Date-time format is MM/DD/YYYY HH:mm A';

    constructor() { }

    ngOnInit() {
        if (this.fieldName) {
            this.dateTimeFieldName = this.fieldName;
        }

        if (this.minDate) {
            this.minimumDate = this.minDate;
        } else {
            this.minimumDate = new Date('01/01/2000');
        }

        if (this.required) {
            this.isRequired = this.required;
        } else {
            this.isRequired = false;
        }

        if (!this.disabled) {
            this.disabled = false;
        }
    }

    ngOnChanges() {
        // setTimeout is used to avoid ngDoCheck error
        setTimeout(() => {
            if (this.resetDateTime === true) {
                if (this.initialDateTime) {
                    this.setInitialDateTime();
                } else {
                    this.dateTimeForm = new FormControl('');

                    if (this.isRequired) {
                        this.dateTimeForm.setValidators([Validators.required]);
                    }
                }
                this.dateTimeResetEvent.emit(false);
            }

            if (this.saveDateTimeAttempt === true) {
                this.dateTimePickerTouched();
                this.dateTimeSaveAttemptEvent.emit(false);
            }
        }, 0);

        if (this.displayDateTime) {
            const displayDateTime = MOMENT(new Date(this.displayDateTime));
            this.dateTimeForm.setValue(displayDateTime);
            this.dateTimeSelection.emit(this.dateTimeForm.value.format('YYYY-MM-DDTHH:mm:ss'));
        }

        if (this.required) {
            this.isRequired = this.required;
        } else {
            this.isRequired = this.required;
        }

        if (this.initialDateTime) {
            this.setInitialDateTime();
        }
    }

    setInitialDateTime() {
        const newDateTime = MOMENT(new Date());
        this.dateTimeForm.setValue(newDateTime);
        this.dateTimeSelection.emit(this.dateTimeForm.value.format('YYYY-MM-DDTHH:mm:ss'));
    }

    onSelectDateTime(dateTimeSelected: any) {
        if (dateTimeSelected.value) {
            const dateTimeMoment = dateTimeSelected.value.format('YYYY-MM-DDTHH:mm:ss');
            this.dateTimeSelection.emit(dateTimeMoment);
        } else {
            this.dateTimeSelection.emit(null);
        }
    }

    dateTimePickerTouched() {
        this.dateTimeForm.markAsTouched();
    }

}
