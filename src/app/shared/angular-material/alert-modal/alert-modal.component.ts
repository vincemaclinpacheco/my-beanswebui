import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    template:
    `
        <form>
            <div class="form-row">
                <div class="col-sm-12 col-md-12">
                    <h1 mat-dialog-title>{{ data.header }}</h1>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="modal-alert d-flex align-items-center">
                        <i *ngIf="data.alertType === 'info'" class="far fa-check-circle"></i>
                        <i *ngIf="data.alertType === 'warning'" class="far fa-exclamation-circle"></i>

                        <p> {{ data.body }} </p>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div mat-dialog-actions>
                        <button type="button" class="alert-mat-button" mat-button (click)="onConfirm()">Okay</button>
                    </div>
                </div>
            </div>
        </form>
    `
})
export class AlertModalViewComponent {

    constructor(
        public dialogRef: MatDialogRef<AlertModalViewComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    onConfirm(): void {
        this.dialogRef.close('OK');
    }

}

@Component({
    selector: 'app-alert-modal',
    templateUrl: './alert-modal.component.html',
    styleUrls: ['./alert-modal.component.scss']
})
export class AlertModalComponent {

    constructor(
        public dialog: MatDialog
    ) { }

    openDialog(headerText: string, bodyText: string, type: string ) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
            width: '500px',
            height: '200px',
            data: {header: headerText, body: bodyText, alertType: type},
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
}
