

export class User {
    id: number;
    userId: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
    role: string;
    lastLoginDate: string;
    loginAttempts: number;

    newPassword: string;
    // tslint:disable-next-line:ban-types
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

    fullName() {
        return `${this.lastName}, ${this.firstName}`;
    }
}

export enum Role {
    USER = 'user',
    ADMIN = 'admin'
}
