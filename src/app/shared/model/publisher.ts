import { Book } from './book';


export class Publisher {
    id: number;
    name: string;
    city: string;
    country: string;
    email: string;
    telephone: string;
    bookSet: Book[];
    
    // tslint:disable-next-line:ban-types
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}