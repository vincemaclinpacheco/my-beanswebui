import { Author } from './author';
import { Publisher } from './publisher';


export class Book {
    id: number;
    title: string;
    format: string;
    price: number;
    publishDate: string;

    author: Author;
    publisher: Publisher;
    
    // tslint:disable-next-line:ban-types
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export enum Format {
    PAPERBACK = 'paperback',
    HARDBACK = 'hardback',
    EBOOK = 'ebook'
}