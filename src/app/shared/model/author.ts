import { Book } from './book';


export class Author {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    bookSet: Book[];
    
    // tslint:disable-next-line:ban-types
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}