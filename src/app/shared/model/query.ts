
export class QueryParam {
    property: string;
    qOperator: string;
    value: string | number;
    orOperands: OrQueryParam[];

    constructor(
        property: string,
        qOperator: string,
        value: string | number,
        orOperands?: OrQueryParam[]
    ) {
        this.property = property;
        this.qOperator = qOperator;
        this.value = value;
        this.orOperands = orOperands;
    }

    appendOrOperand(operand: OrQueryParam) {
        if (this.orOperands === null || this.orOperands === undefined) {
            this.orOperands = new Array<OrQueryParam>();
        }
        this.orOperands.push(operand);
    }
}

export class OrQueryParam {

    constructor(private property: string,
                private qOperator: Operator,
                private value: any) { }
}

export enum Operator {
    BTWN = 'BTWN',
    EQ = 'EQ',
    EQIC = 'EQ_IC',
    NE = 'NE',
    GT = 'GT',
    GTE = 'GTE',
    IN = 'IN',
    LIKE = 'LIKE',
    LT = 'LT',
    LTE = 'LTE',
}

