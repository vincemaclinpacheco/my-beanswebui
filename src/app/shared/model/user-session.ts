import { User } from './user';


export class UserSession {
    id: number;
    user: User
    token: string;
}
