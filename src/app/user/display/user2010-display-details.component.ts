import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { User, Role } from 'src/app/shared/model/user';
import { MOMENT } from 'src/app/shared/angular-material/angular-material.module';

@Component({
  selector: 'app-user2010-display-details',
  templateUrl: './user2010-display-details.component.html',
  styleUrls: ['./user2010-display-details.component.scss']
})
export class User2010DisplayDetailsComponent implements OnInit {

    @Input()
    userToDisplay: User;

    @Input()
    formDisable: boolean;

    public user2010Form: FormGroup;
    public user2010FormControl;

    public disabled = false;

    public Role = Role;
    public roleKeys = Object.keys(Role);


  constructor(
    private fb: FormBuilder
  ) {
    this.createForm();
   }

  ngOnInit(): void {
  
  }


  ngOnChanges() {
    if (this.userToDisplay) {
        this.user2010FormControl['id'].setValue(this.userToDisplay.id);
        this.user2010FormControl['userId'].setValue(this.userToDisplay.userId);
        this.user2010FormControl['firstName'].setValue(this.userToDisplay.firstName);
        this.user2010FormControl['lastName'].setValue(this.userToDisplay.lastName);
        this.user2010FormControl['role'].setValue(this.userToDisplay.role);
        this.user2010FormControl['email'].setValue(this.userToDisplay.email);
        const lastLoginDateMoment = MOMENT(new Date(this.userToDisplay.lastLoginDate));
        this.user2010FormControl['lastLoginDate'].setValue(lastLoginDateMoment.format('MM/DD/YYYY HH:mm:ss'));

    }

    console.log(this.formDisable);
}

  createForm() {
    this.user2010Form = this.fb.group({
        id: 0,
        userId: '',
        firstName: '',
        lastName: '',
        role: '',
        email: '',
        lastLoginDate: ''
    });
    this.user2010FormControl = this.user2010Form.controls;
}

onSelectRole(e: any) {}

user2010FormValues() {
    this.markFormGroupTouched(this.user2010Form);
    return this.user2010Form.value;
}

isFormValid() {
    return this.user2010Form.valid;
}

markFormGroupTouched(formGroup: FormGroup) {
    (Object as any).values(formGroup.controls).forEach(control => {
        control.markAsTouched();
            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
}

}
