import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { UserService } from 'src/app/shared/service/user.service';
import { User } from 'src/app/shared/model/user';
import { User2010DisplayDetailsComponent } from './user2010-display-details.component';
import { AlertModalViewComponent } from 'src/app/shared/angular-material/alert-modal/alert-modal.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-user2000-display',
  templateUrl: './user2000-display.component.html',
  styleUrls: ['./user2000-display.component.scss']
})
export class User2000DisplayComponent implements OnInit {

    @ViewChild(User2010DisplayDetailsComponent)
    private user2010: User2010DisplayDetailsComponent;

    public userToDisplay: User;
    public userToUpdate: User;
    public formDisable = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.formDisable = true;
    const id = this.activatedRoute.snapshot.params['id'];
    this.getUser(id);
  }

  getUser(id: number) {
    this.userService
    .getUser(id)
    .subscribe(result => {
       this.userToDisplay = result;
       console.log(this.userToDisplay);
    });
  }

  onClickEdit() {
    this.formDisable = false;
  }

  onClickReset() {
    this.formDisable = true;
  }

  onClickSave() {
    this.userToUpdate = new User();
    this.userToUpdate = this.user2010.user2010FormValues();
    this.userToUpdate.lastLoginDate = this.userToDisplay.lastLoginDate;
    console.log(this.userToUpdate);
    if (this.user2010.isFormValid()) {
            this.userService
            .updateUser(this.userToUpdate)
            .subscribe((result: {success: boolean; contentList: User[]; totalElements: number; }) => {
                if (result.success) {
                    // console.log('success');
                    this.userToDisplay = new User();
                    this.userToDisplay = result.contentList[0];
                    this.formDisable = true;
                    this.openSuccessDialog('Edit User', `Successfuly updated user ${result.contentList[0].userId}`, 'success');
                    } else {
                        // console.log('fail');
                        // this.modalHeaderText = this.messageService.getValue('userWarning');
                        // this.modalBodyText = 'Update Failed';
                        // this.modalType = 'warning';
                        // this.alertModal.openDialog(this.modalHeaderText, this.modalBodyText, this.modalType);
                        this.openFailDialog('User', `Failed to update user.`, 'warning');
                    }
                });

        } else {
        // this.modalHeaderText = this.messageService.getValue('userWarning');
        // this.modalBodyText = 'Invalid Form';
        // this.modalType = 'warning';
        // this.alertModal.openDialog(this.modalHeaderText, this.modalBodyText, this.modalType);
    }
  }

    openSuccessDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.backToList();
        console.log('The dialog was closed');
        });
    }

    openFailDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
            const id = this.activatedRoute.snapshot.params['id'];
            this.getUser(id);
        });
    }

    backToList() {
        // this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/user/list');
    }

}
