import { Component, OnInit, NgZone } from '@angular/core';
import { UserService } from 'src/app/shared/service/user.service';
import { User } from 'src/app/shared/model/user';
import { GridOptions } from 'ag-grid-community';
import { Router } from '@angular/router';
import { DateRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/date-renderer.component';
import { DatetimeRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/datetime-renderer.component';

@Component({
  selector: 'app-user1010-list-table',
  templateUrl: './user1010-list-table.component.html',
  styleUrls: ['./user1010-list-table.component.scss']
})
export class User1010ListTableComponent implements OnInit {

public gridOptions: GridOptions;
public gridApi;
public gridColumnApi;


public totalElements: number;
public userList: User[] = [];
public selectedUser: User;

rowData = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 }
];

  constructor(
      private userService: UserService,
      private router: Router,
      private ngZone: NgZone
      ) {
            this.gridOptions = { 
                columnDefs: [
                    { headerName: 'User ID', field: 'userId', width: 130, sortable: true },
                    { headerName: 'Name', width: 250, colId: 'fullName', valueGetter: function nameConcat(params) {
                        return params.data.firstName + ' ' + params.data.lastName;
                    }},
                    { headerName: 'Role', field: 'role', width: 130 },
                    { headerName: 'Email', field: 'email', width: 270 },
                    { headerName: 'Last Login Date', field: 'lastLoginDate', width: 270, cellRenderer: 'dateTimeRenderer' }
                 ],
                 enableColResize: true,
                 rowSelection: 'single',
                 frameworkComponents : {
                    dateRenderer: DateRendererComponent,
                    dateTimeRenderer: DatetimeRendererComponent
                }
            };
            // this.columnDefs = [
            //     {headerName: 'Make', field: 'make' },
            //     {headerName: 'Model', field: 'model' },
            //     {headerName: 'Price', field: 'price'}
            // ];
       }

  ngOnInit(): void {
    this.listUsers(0, 10);
  }

  listUsers(pageNo: number, pageSize: number) {
        this.userService
            .listUsers(pageNo, pageSize)
            .subscribe((result: {success: boolean; contentList: User[]; totalElements: number; }) => {
                this.userList = result.contentList;
                this.totalElements = result.totalElements;
                console.log(this.userList);
            });
    }

    onSelectUser() {
        const selectedNode = this.gridApi.getSelectedNodes();
        const selectedData = selectedNode.map( node => node.data );
        this.selectedUser = selectedData[0];
        this.changeRoute(this.selectedUser.id);
   
    }

    changeRoute(id: number) {
        // this.router.navigate(['user/display/' + id]);
        this.ngZone.run(() => this.router.navigate(['user/display/' + id])).then();
    }

  refreshGrid() {
    this.gridApi.refreshCells();
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

}
