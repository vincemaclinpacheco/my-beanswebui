import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-user1000-list',
  templateUrl: './user1000-list.component.html',
  styleUrls: ['./user1000-list.component.scss']
})
export class User1000ListComponent implements OnInit {

    public resetToSearch = false;

    constructor(
        private userService: UserService
    ) { }

    ngOnInit(): void {

    }

    ngAfterViewInit() {
    
    }

}
