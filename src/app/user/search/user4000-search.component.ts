import { Component, OnInit, ViewChild } from '@angular/core';
import { User4010SearchPanelComponent } from './user4010-search-panel.component';
import { QueryParam, Operator } from 'src/app/shared/model/query';
import { User } from 'src/app/shared/model/user';
import { UserService } from 'src/app/shared/service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user4000-search',
  templateUrl: './user4000-search.component.html',
  styleUrls: ['./user4000-search.component.scss']
})
export class User4000SearchComponent implements OnInit {

    @ViewChild(User4010SearchPanelComponent)
    private user4010Form: User4010SearchPanelComponent;

    public queryParams: QueryParam[] = [];
    public searchedUserList: User[] = [];
    public pageSize = 25;
    public totalElements: number;

    public userForm: any;

    constructor(
        private userService: UserService,
        private router: Router
    ) { }

    ngOnInit(): void {

    }

    onClickReset() {
        this.user4010Form.onReset();
    }

    onClickSearch() {
        this.queryParams = [];
        this.searchedUserList = [];

        this.userForm = this.user4010Form.user4010FormValues();
        /* User Form Parameter */
        if (this.userForm.userId !== '') {
            const userIdParam = new QueryParam('userId', Operator.EQIC, this.userForm.userId);
            this.queryParams.push(userIdParam);
        }

        if (this.userForm.firstName !== '') {
            let firstNameParam: QueryParam;
            if (this.userForm.firstName.includes('*')) {
                firstNameParam = new QueryParam('firstName', Operator.LIKE, this.userForm.firstName);
            } else {
                firstNameParam = new QueryParam('firstName', Operator.EQIC, this.userForm.firstName);
            }
            this.queryParams.push(firstNameParam);
        }

        if (this.userForm.lastName !== '') {
            const lastNameParam = new QueryParam('lastName', Operator.LIKE, this.userForm.lastName);
            this.queryParams.push(lastNameParam);
        }

        if (this.userForm.role !== '') {
            const roleParam = new QueryParam('role', Operator.EQIC, this.userForm.role);
            this.queryParams.push(roleParam);
        }

        if (this.userForm.email !== '') {
            const emailParam = new QueryParam('email', Operator.EQIC, this.userForm.email);
            this.queryParams.push(emailParam);
        }

        
        if (this.userForm.lastLoginStartDate !== '') {
            const saleStartDateParam = new QueryParam('lastLoginDate', Operator.GTE, this.userForm.lastLoginStartDate);
            this.queryParams.push(saleStartDateParam);
        }

        if (this.userForm.lastLoginEndDate !== '') {
            const saleEndDateParam = new QueryParam('lastLoginDate', Operator.LTE, this.userForm.lastLoginEndDate);
            this.queryParams.push(saleEndDateParam);
        }

        this.searchUser(0, this.pageSize);

    }

    searchUser(page: number, pageSize: number) {
        this.userService
        .searchUser(this.queryParams, page, pageSize)
        .subscribe((result: any) => {
            this.searchedUserList = result.contentList;
            this.totalElements = result.totalElements;
            console.log(this.searchedUserList);
        });
    }

    checkUserResultLength() {
        if (this.searchedUserList.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    onClickBack() {
        this.searchedUserList = [];
    }

}
