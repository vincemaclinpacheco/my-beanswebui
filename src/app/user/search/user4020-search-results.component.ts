import { Component, OnInit, Input, NgZone } from '@angular/core';
import { User } from 'src/app/shared/model/user';
import { GridOptions } from 'ag-grid-community';
import { UserService } from 'src/app/shared/service/user.service';
import { Router } from '@angular/router';
import { DateRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/date-renderer.component';
import { DatetimeRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/datetime-renderer.component';

@Component({
  selector: 'app-user4020-search-results',
  templateUrl: './user4020-search-results.component.html',
  styleUrls: ['./user4020-search-results.component.scss']
})
export class User4020SearchResultsComponent implements OnInit {

    @Input()
    searchedUserList: User[] = [];

    @Input()
    totalElements: number;

    public gridOptions: GridOptions;
    public gridApi;
    public gridColumnApi;

    public selectedUser: User;

    constructor(
        private userService: UserService,
        private router: Router,
        private ngZone: NgZone
    ) {
        this.gridOptions = { 
            columnDefs: [
                { headerName: 'User ID', field: 'userId', width: 130, sortable: true },
                { headerName: 'Name', width: 250, colId: 'fullName', valueGetter: function nameConcat(params) {
                    return params.data.firstName + ' ' + params.data.lastName;
                }},
                { headerName: 'Role', field: 'role', width: 130 },
                { headerName: 'Email', field: 'email', width: 270 },
                { headerName: 'Last Login Date', field: 'lastLoginDate', width: 270, cellRenderer: 'dateTimeRenderer' }
             ],
             enableColResize: true,
             rowSelection: 'single',
             frameworkComponents : {
                dateRenderer: DateRendererComponent,
                dateTimeRenderer: DatetimeRendererComponent
            }
        };
     }

    ngOnInit(): void {
    }

    onSelectUser() {
        const selectedNode = this.gridApi.getSelectedNodes();
        const selectedData = selectedNode.map( node => node.data );
        this.selectedUser = selectedData[0];
        this.changeRoute(this.selectedUser.id);
   
    }

    changeRoute(id: number) {
        // this.router.navigate(['user/display/' + id]);
        this.ngZone.run(() => this.router.navigate(['user/display/' + id])).then();
    }

  refreshGrid() {
    this.gridApi.refreshCells();
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

}
