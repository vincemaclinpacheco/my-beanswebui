import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DateRange } from 'src/app/shared/model/form';
import { Role } from 'src/app/shared/model/user';

@Component({
  selector: 'app-user4010-search-panel',
  templateUrl: './user4010-search-panel.component.html',
  styleUrls: ['./user4010-search-panel.component.scss']
})
export class User4010SearchPanelComponent implements OnInit {

    public user4010Form: FormGroup;
    public user4010FormControl;

    public startDateLabel = 'Start Date';
    public endDateLabel = 'End Date';
    public resetAllDate = false;

    public lastLoginDateTxt = 'Last Login Date';

    public Role = Role;
    public roleKeys = Object.keys(Role);

    constructor(
        private fb: FormBuilder
      ) {
        this.createForm();
       }

    ngOnInit(): void {
    }

    createForm() {
        this.user4010Form = this.fb.group({
            id: 0,
            userId: '',
            firstName: '',
            lastName: '',
            role: '',
            email: '',
            lastLoginStartDate: '',
            lastLoginEndDate: ''
        });
        this.user4010FormControl = this.user4010Form.controls;
    }

    user4010FormValues() {
        return this.user4010Form.value;
    }

    onReset() {
        this.createForm();
    }

    onSelectLastLoginDate(dateRangeSelected: DateRange) {
        console.log(dateRangeSelected);
        if (dateRangeSelected) {
            this.user4010FormControl['lastLoginStartDate'].setValue(`${dateRangeSelected.begin}T00:00:00`);
            this.user4010FormControl['lastLoginEndDate'].setValue(`${dateRangeSelected.end}T11:59:59`);
        } else {
            this.user4010FormControl['lastLoginStartDate'].setValue('');
            this.user4010FormControl['lastLoginEndDate'].setValue('');
        }
    }

    onSelectRole(e: any) {}

    onResetAllDate(status: boolean) {
        if (this.resetAllDate === true) {
            this.resetAllDate = status;
        }
    }




}
