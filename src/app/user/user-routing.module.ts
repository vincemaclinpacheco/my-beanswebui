import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { User1000ListComponent } from './list/user1000-list.component';
import { User2000DisplayComponent } from './display/user2000-display.component';
import { User3000FormComponent } from './form/user3000-form.component';
import { User4000SearchComponent } from './search/user4000-search.component';


const USER_ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: '',
        component: UserComponent,
        children: [
            {
                path:'list',
                component: User1000ListComponent
            },
            {
                path:'display/:id',
                component: User2000DisplayComponent
            },
            {
                path:'new',
                component: User3000FormComponent
            },
            {
                path:'search',
                component: User4000SearchComponent
            },
        ]
    }
    
];

@NgModule({
  imports: [RouterModule.forChild(USER_ROUTES)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
