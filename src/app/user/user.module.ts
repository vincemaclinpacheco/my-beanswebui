import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { UserRoutingModule } from './user-routing.module';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { AgGridCustomModule } from '../shared/ag-grid-custom/ag-grid-custom.module';
import { UserComponent } from './user.component';
import { User1000ListComponent } from './list/user1000-list.component';
import { User4000SearchComponent } from './search/user4000-search.component';
import { User3000FormComponent } from './form/user3000-form.component';
import { User2000DisplayComponent } from './display/user2000-display.component';
import { User1010ListTableComponent } from './list/user1010-list-table.component';
import { User2010DisplayDetailsComponent } from './display/user2010-display-details.component';
import { User3010FormPanelComponent } from './form/user3010-form-panel.component';
import { User4010SearchPanelComponent } from '../user/search/user4010-search-panel.component';
import { User4020SearchResultsComponent } from './search/user4020-search-results.component';


@NgModule({
  declarations: [
      User1000ListComponent, UserComponent, User4000SearchComponent,
      User3000FormComponent, User2000DisplayComponent, User1010ListTableComponent,
      User2010DisplayDetailsComponent, User3010FormPanelComponent, User4010SearchPanelComponent, User4020SearchResultsComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    AngularMaterialModule,
    AgGridCustomModule,
    SharedModule
  ]
})
export class UserModule { }
