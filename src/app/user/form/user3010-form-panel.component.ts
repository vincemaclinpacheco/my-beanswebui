import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl, FormGroupDirective, NgForm } from '@angular/forms';
import { User, Role } from 'src/app/shared/model/user';
import { UserService } from 'src/app/shared/service/user.service';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when the parent is invalid */
class CrossFieldErrorMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      return control.dirty && form.invalid;
    }
}

@Component({
  selector: 'app-user3010-form-panel',
  templateUrl: './user3010-form-panel.component.html',
  styleUrls: ['./user3010-form-panel.component.scss']
})
export class User3010FormPanelComponent implements OnInit {

public Role = Role;
public roleKeys = Object.keys(Role);


public user3010Form: FormGroup;
public user3010FormControl;

public hide = false;

errorMatcher = new CrossFieldErrorMatcher();

public errorMessage = 'Required Field';

public formDisable: false;

public newUser = new User();

    constructor(
        private fb: FormBuilder,
        private userService: UserService
    ) {
        this.createForm();
    }

    ngOnInit(): void {
    }

    createForm() {
        this.user3010Form = this.fb.group({
            userId: ['', [Validators.required]],
            password: ['',
                [Validators.required,
                this.minLengthValidator,
                this.alphaNumericValidator,
                this.noRepeatingValidator,
                this.checkUsernameValidator(this.newUser.userId)
                ]
            ],
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            role: ['', [Validators.required]],
            email: '',
            confirmPassword: ['', [Validators.required]]
        }, { validator: this.checkMatchValidator });
        this.user3010FormControl = this.user3010Form.controls;
    }

    user3010FormValues() {
        this.markFormGroupTouched(this.user3010Form);
        return this.user3010Form.value;
    }

    isFormValid() {
        return this.user3010Form.valid;
    }

    markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
        control.markAsTouched();
        if (control.controls) {
            this.markFormGroupTouched(control);
        }
        });
    }

    minLengthValidator(control: FormControl) {
        if (control.value.length >= 8) {
            return null;
        } else {
            return {
                minLength: true
            };
        }
    }

    alphaNumericValidator(control: FormControl) {
        const alphaNumericRegexp = /^(?=(.*[a-zA-Z].*){1,})(?=.*\d.*)[a-zA-Z0-9\S]{0,}$/;
        if (alphaNumericRegexp.test(control.value)) {
            return null;
        } else {
            return {
                invalidPattern: true
            };
        }
    }

    /* tslint:disable-next-line:ban-types */
    oldPasswordValidator(password: String): ValidatorFn {
        return (control: AbstractControl): { [key: string]: boolean } | null => {
            if (control.value !== password) {
                return { incorrectPassword: true };
            }
            return null;
        };
    }

    /* tslint:disable-next-line:ban-types */
    checkUsernameValidator(userName: String): ValidatorFn {
        return (control: AbstractControl): { [key: string]: boolean } | null => {
            if (control.value.includes(userName) || control.value === '') {
                return { containsUsername: true };
            }
            return null;
        };
    }

    noRepeatingValidator(control: FormControl) {
        const noRepeatingRegexp = /^(?!.*(\w)\1{2,}).+$/;
        if (noRepeatingRegexp.test(control.value)) {
                return null;
            } else {
            return {
                repeatingChar: true
            };
        }
    }

    checkMatchValidator(group: FormGroup) {
        const newPass = group.controls.password.value;
        const confirmPass = group.controls.confirmPassword.value;
        return newPass === confirmPass ? null : {notMatch: true};
    }

    onChangeConfirm() {
      this.user3010FormControl['confirmPassword'].markAsTouched();
      console.log('a');
    }

    onChangeUserId(userId: string) {
    this.newUser.userId = userId;
    this.user3010FormControl['password'].setValidators(
        [Validators.required,
        this.minLengthValidator,
        this.alphaNumericValidator,
        this.noRepeatingValidator,
        this.checkUsernameValidator(this.newUser.userId)
    ]);
    }

    onSelectRole(e: any) {}

    onReset() {
        this.createForm();
        // console.log(this.user3010Form.hasError('notMatch'));
    }

}
