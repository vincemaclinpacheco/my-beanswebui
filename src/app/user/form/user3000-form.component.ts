import { Component, OnInit, ViewChild } from '@angular/core';
import { User3010FormPanelComponent } from './user3010-form-panel.component';
import { User } from 'src/app/shared/model/user';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/service/user.service';
import { MatDialog } from '@angular/material/dialog';
import { AlertModalViewComponent } from 'src/app/shared/angular-material/alert-modal/alert-modal.component';

@Component({
  selector: 'app-user3000-form',
  templateUrl: './user3000-form.component.html',
  styleUrls: ['./user3000-form.component.scss']
})
export class User3000FormComponent implements OnInit {

    @ViewChild(User3010FormPanelComponent)
    private user3010: User3010FormPanelComponent;

    public newUser: User;

    constructor(
        private router: Router,
        private userService: UserService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
    }

    onClickReset() {
        this.user3010.onReset();
    }

    onClickAdd() {
        this.newUser = new User();
        this.newUser = this.user3010.user3010FormValues();
            if (this.user3010.isFormValid()) {
                console.log(this.newUser);
                this.newUser.loginAttempts = 0;
                this.userService.createUser(this.newUser)
                    .subscribe((result: {success: boolean; contentList: User[]; totalElements: number}) => {
                        if (result.success) {
                            console.log('success');
                            this.openSuccessDialog('Create User', `Successfuly created user ${result.contentList[0].userId}`, 'success');
                        } else {
                                console.log('fail');
                            this.openFailDialog('User', `Failed to create new user.`, 'warning');
                        }
                    });
            } else {

            }
    }

    openSuccessDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.backToList();
        console.log('The dialog was closed');
        });
    }

    openFailDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.onClickReset();
        console.log('The dialog was closed');
        });
    }

    backToList() {
        // this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/user/list');
    }


}
