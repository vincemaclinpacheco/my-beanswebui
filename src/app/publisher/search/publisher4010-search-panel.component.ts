import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-publisher4010-search-panel',
  templateUrl: './publisher4010-search-panel.component.html',
  styleUrls: ['./publisher4010-search-panel.component.scss']
})
export class Publisher4010SearchPanelComponent implements OnInit {

    public publisher4010Form: FormGroup;
    public publisher4010FormControl;

    constructor(
        private fb: FormBuilder
    ) {
        this.createForm();
     }

    ngOnInit(): void {
    }

    createForm() {
        this.publisher4010Form = this.fb.group({
            id: 0,
            name: '',
            city: '',
            country: '',
        });
        this.publisher4010FormControl = this.publisher4010Form.controls;
    }

    publisher4010FormValues() {
        return this.publisher4010Form.value;
    }

    onReset() {
        this.createForm();
    }

}
