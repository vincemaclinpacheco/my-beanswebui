import { Component, OnInit, Input, NgZone } from '@angular/core';
import { Publisher } from 'src/app/shared/model/publisher';
import { PublisherService } from 'src/app/shared/service/publisher.service';
import { GridOptions } from 'ag-grid-community';
import { Router } from '@angular/router';
import { DateRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/date-renderer.component';

@Component({
  selector: 'app-publisher4020-search-results',
  templateUrl: './publisher4020-search-results.component.html',
  styleUrls: ['./publisher4020-search-results.component.scss']
})
export class Publisher4020SearchResultsComponent implements OnInit {

    @Input()
    searchedPublisherList: Publisher[] = [];

    @Input()
    totalElements: number;

    public gridOptions: GridOptions;
    public gridApi;
    public gridColumnApi;

    public selectedPublisher: Publisher;


    constructor(
        private publisherService: PublisherService,
        private router: Router,
        private ngZone: NgZone
    ) {
        this.gridOptions = { 
            columnDefs: [
                { headerName: 'Name', field: 'name', sortable: true, width: 250 },
                { headerName: 'City', field: 'city', sortable: true, width: 130 },
                { headerName: 'Country', field: 'country', sortable: true, width: 130 },
                { headerName: 'Email', field: 'email', sortable: true, width: 250 },
                { headerName: 'Telephone', field: 'telephone', sortable: true, width: 130 }
    
             ],
             enableColResize: true,
             rowSelection: 'single',
             frameworkComponents : {
                dateRenderer: DateRendererComponent
            }
        };
     }

    ngOnInit(): void {
    }

    onSelectPublisher() {
        const selectedNode = this.gridApi.getSelectedNodes();
        const selectedData = selectedNode.map( node => node.data );
        this.selectedPublisher = selectedData[0];
        this.changeRoute(this.selectedPublisher.id);
   
    }

    changeRoute(id: number) {
        // this.router.navigate(['user/display/' + id]);
        this.ngZone.run(() => this.router.navigate(['publisher/display/' + id])).then();
    }

    refreshGrid() {
        this.gridApi.refreshCells();
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

}
