import { Component, OnInit, ViewChild } from '@angular/core';
import { Publisher4010SearchPanelComponent } from './publisher4010-search-panel.component';
import { QueryParam, Operator } from 'src/app/shared/model/query';
import { Publisher } from 'src/app/shared/model/publisher';
import { PublisherService } from 'src/app/shared/service/publisher.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-publisher4000-search',
  templateUrl: './publisher4000-search.component.html',
  styleUrls: ['./publisher4000-search.component.scss']
})
export class Publisher4000SearchComponent implements OnInit {

    @ViewChild(Publisher4010SearchPanelComponent)
    private publisher4010Form: Publisher4010SearchPanelComponent;

    public queryParams: QueryParam[] = [];
    public searchedPublisherList: Publisher[] = [];
    public pageSize = 25;
    public totalElements: number;

    public publisherForm: any;

    constructor(
        private publisherService: PublisherService,
        private router: Router
    ) { }

    ngOnInit(): void {
    }

    onClickReset() {
        this.publisher4010Form.onReset();
    }

    onClickSearch() {
        this.queryParams = [];
        this.searchedPublisherList = [];

        this.publisherForm = this.publisher4010Form.publisher4010FormValues();
        /* Book Form Parameter */
        if (this.publisherForm.name !== '') {
            let nameParam: QueryParam;
            this.publisherForm.name = this.publisherForm.name.trim();
            this.publisherForm.name = `*${this.publisherForm.name}*`
  
            nameParam = new QueryParam('name', Operator.LIKE, this.publisherForm.name);

            this.queryParams.push(nameParam);
        }

        if (this.publisherForm.city !== '') {
            let cityParam: QueryParam;
            this.publisherForm.city = this.publisherForm.city.trim();
            this.publisherForm.city = `*${this.publisherForm.city}*`
  
            cityParam = new QueryParam('city', Operator.LIKE, this.publisherForm.city);

            this.queryParams.push(cityParam);
        }

        if (this.publisherForm.country !== '') {
            let countryParam: QueryParam;
            this.publisherForm.country = this.publisherForm.country.trim();
            this.publisherForm.city = `*${this.publisherForm.country}*`
  
            countryParam = new QueryParam('country', Operator.LIKE, this.publisherForm.country);

            this.queryParams.push(countryParam);
        }

        // if (this.publisherForm.city !== '') {
        //     const cityParam = new QueryParam('city', Operator.LIKE, this.publisherForm.city);
        //     this.queryParams.push(cityParam);
        // }

        // if (this.publisherForm.role !== '') {
        //     const roleParam = new QueryParam('role', Operator.EQIC, this.publisherForm.role);
        //     this.queryParams.push(roleParam);
        // }

        // if (this.publisherForm.email !== '') {
        //     const emailParam = new QueryParam('email', Operator.EQIC, this.publisherForm.email);
        //     this.queryParams.push(emailParam);
        // }

        
        // if (this.publisherForm.lastLoginStartDate !== '') {
        //     const saleStartDateParam = new QueryParam('lastLoginDate', Operator.GTE, this.publisherForm.lastLoginStartDate);
        //     this.queryParams.push(saleStartDateParam);
        // }

        // if (this.publisherForm.lastLoginEndDate !== '') {
        //     const saleEndDateParam = new QueryParam('lastLoginDate', Operator.LTE, this.publisherForm.lastLoginEndDate);
        //     this.queryParams.push(saleEndDateParam);
        // }

        this.searchPublisher(0, this.pageSize);

    }

    searchPublisher(page: number, pageSize: number) {
        this.publisherService
        .search(this.queryParams, page, pageSize)
        .subscribe((result: any) => {
            this.searchedPublisherList = result.contentList;
            this.totalElements = result.totalElements;
            console.log(this.searchedPublisherList);
        });
    }

    checkPublisherResultLength() {
        if (this.searchedPublisherList.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    onClickBack() {
        this.searchedPublisherList = [];
    }

}
