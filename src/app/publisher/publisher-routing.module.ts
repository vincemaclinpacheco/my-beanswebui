import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublisherComponent } from './publisher.component';
import { Publisher2000DisplayComponent } from './display/publisher2000-display.component';
import { Publisher3000FormComponent } from './form/publisher3000-form.component';
import { Publisher4000SearchComponent } from './search/publisher4000-search.component';
import { Publisher1000ListComponent } from './list/publisher1000-list.component';


const PUBLISHER_ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: '',
        component: PublisherComponent,
        children: [
            {
                path: 'list',
                component: Publisher1000ListComponent
            },
            {
                path:'display/:id',
                component: Publisher2000DisplayComponent
            },
            {
                path:'new',
                component: Publisher3000FormComponent
            },
            {
                path:'search',
                component: Publisher4000SearchComponent
            },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(PUBLISHER_ROUTES)],
  exports: [RouterModule]
})
export class PublisherRoutingModule { }
