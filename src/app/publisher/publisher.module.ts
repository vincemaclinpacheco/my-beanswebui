import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { AgGridCustomModule } from '../shared/ag-grid-custom/ag-grid-custom.module';
import { PublisherRoutingModule } from './publisher-routing.module';
import { PublisherComponent } from './publisher.component';
import { Publisher1000ListComponent } from './list/publisher1000-list.component';
import { Publisher1010ListTableComponent } from './list/publisher1010-list-table.component';
import { Publisher2000DisplayComponent } from './display/publisher2000-display.component';
import { Publisher2010DisplayDetailsComponent } from './display/publisher2010-display-details.component';
import { Publisher3000FormComponent } from './form/publisher3000-form.component';
import { Publisher3010FormPanelComponent } from './form/publisher3010-form-panel.component';
import { Publisher4000SearchComponent } from './search/publisher4000-search.component';
import { Publisher4010SearchPanelComponent } from './search/publisher4010-search-panel.component';
import { Publisher4020SearchResultsComponent } from './search/publisher4020-search-results.component';

@NgModule({
  declarations: [PublisherComponent, Publisher1000ListComponent, Publisher1010ListTableComponent, 
    Publisher2000DisplayComponent, Publisher2010DisplayDetailsComponent, Publisher3000FormComponent, 
    Publisher3010FormPanelComponent, Publisher4000SearchComponent, Publisher4010SearchPanelComponent,
    Publisher4020SearchResultsComponent],
  imports: [
    CommonModule,
    PublisherRoutingModule,
    AngularMaterialModule,
    AgGridCustomModule,
    SharedModule
  ],
  exports: [
      Publisher2000DisplayComponent,
      Publisher2010DisplayDetailsComponent,
      Publisher3000FormComponent,
      Publisher3010FormPanelComponent
  ]
})
export class PublisherModule { }
