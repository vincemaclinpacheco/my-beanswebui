import { Component, OnInit, Input } from '@angular/core';
import { Publisher } from 'src/app/shared/model/publisher';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-publisher2010-display-details',
  templateUrl: './publisher2010-display-details.component.html',
  styleUrls: ['./publisher2010-display-details.component.scss']
})
export class Publisher2010DisplayDetailsComponent implements OnInit {

    @Input()
    publisherToDisplay: Publisher;

    @Input()
    formDisable: boolean;

    public publisher2010Form: FormGroup;
    public publisher2010FormControl;

    public disabled = false;

    constructor(
        private fb: FormBuilder
    ) {
        this.createForm();
     }

     ngOnInit(): void {
    }

    ngOnChanges() {
        if (this.publisherToDisplay) {
            this.publisher2010FormControl['id'].setValue(this.publisherToDisplay.id);
            this.publisher2010FormControl['name'].setValue(this.publisherToDisplay.name);
            this.publisher2010FormControl['city'].setValue(this.publisherToDisplay.city);
            this.publisher2010FormControl['country'].setValue(this.publisherToDisplay.country);
            this.publisher2010FormControl['email'].setValue(this.publisherToDisplay.email);
            this.publisher2010FormControl['telephone'].setValue(this.publisherToDisplay.telephone);
        }
    
        console.log(this.formDisable);
    }

    createForm() {
        this.publisher2010Form = this.fb.group({
            id: 0,
            name: '',
            city: '',
            country: '',
            email: '',
            telephone: ''
        });
        this.publisher2010FormControl = this.publisher2010Form.controls;
    }

    publisher2010FormValues() {
        this.markFormGroupTouched(this.publisher2010Form);
        return this.publisher2010Form.value;
    }
    
    isFormValid() {
        return this.publisher2010Form.valid;
    }
    
    markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
            control.markAsTouched();
                if (control.controls) {
                    this.markFormGroupTouched(control);
                }
            });
    }

}
