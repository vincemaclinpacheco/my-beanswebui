import { Component, OnInit, ViewChild } from '@angular/core';
import { Publisher2010DisplayDetailsComponent } from './publisher2010-display-details.component';
import { PublisherService } from 'src/app/shared/service/publisher.service';
import { Publisher } from 'src/app/shared/model/publisher';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AlertModalViewComponent } from 'src/app/shared/angular-material/alert-modal/alert-modal.component';

@Component({
  selector: 'app-publisher2000-display',
  templateUrl: './publisher2000-display.component.html',
  styleUrls: ['./publisher2000-display.component.scss']
})
export class Publisher2000DisplayComponent implements OnInit {

    @ViewChild(Publisher2010DisplayDetailsComponent)
    private publisher2010: Publisher2010DisplayDetailsComponent;

    public publisherToDisplay: Publisher;
    public publisherToUpdate: Publisher;
    public formDisable = true;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private publisherService: PublisherService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
        const id = this.activatedRoute.snapshot.params['id'];
        this.getPublisher(id);
    }

    getPublisher(id: number) {
        this.publisherService
        .get(id)
        .subscribe(result => {
            this.publisherToDisplay = result;
            console.log(this.publisherToDisplay);
        });
    }

    onClickEdit() {
        this.formDisable = false;
    }

    onClickReset() {
        this.formDisable = true;
    }

    onClickSave() {
        this.publisherToUpdate = new Publisher();
        this.publisherToUpdate = this.publisher2010.publisher2010FormValues();
        this.publisherToUpdate.bookSet = this.publisherToDisplay.bookSet;
        console.log(this.publisherToUpdate);
        if (this.publisher2010.isFormValid()) {
            this.publisherService
            .update(this.publisherToUpdate)
            .subscribe((result: {success: boolean; contentList: Publisher[]; totalElements: number; }) => {
                if (result.success) {
                    // console.log('success');
                    this.publisherToUpdate = new Publisher();
                    this.publisherToUpdate = result.contentList[0];
                    this.formDisable = true;
                    this.openSuccessDialog('Edit User', `Successfuly updated publisher`, 'success');
                    } else {
                        // console.log('fail');
                        // this.modalHeaderText = this.messageService.getValue('userWarning');
                        // this.modalBodyText = 'Update Failed';
                        // this.modalType = 'warning';
                        // this.alertModal.openDialog(this.modalHeaderText, this.modalBodyText, this.modalType);
                        this.openFailDialog('User', `Failed to update publisher.`, 'warning');
                    }
                });
        }
    }

    openSuccessDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.backToList();
        console.log('The dialog was closed');
        });
    }

    openFailDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
            const id = this.activatedRoute.snapshot.params['id'];
            this.getPublisher(id);
        });
    }

    backToList() {
        // this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/publisher/list');
    }

}
