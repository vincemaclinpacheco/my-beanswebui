import { Component, OnInit, ViewChild } from '@angular/core';
import { Publisher3010FormPanelComponent } from './publisher3010-form-panel.component';
import { Publisher } from 'src/app/shared/model/publisher';
import { PublisherService } from 'src/app/shared/service/publisher.service';
import { AlertModalViewComponent } from 'src/app/shared/angular-material/alert-modal/alert-modal.component';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-publisher3000-form',
  templateUrl: './publisher3000-form.component.html',
  styleUrls: ['./publisher3000-form.component.scss']
})
export class Publisher3000FormComponent implements OnInit {

    @ViewChild(Publisher3010FormPanelComponent)
    private publisher3010: Publisher3010FormPanelComponent;

    public newPublisher: Publisher;

    constructor(
        private router: Router,
        private publisherService: PublisherService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
    }

    onClickReset() {
        this.publisher3010.onReset();
    }

    onClickAdd() {
        this.newPublisher = new Publisher();
        this.newPublisher = this.publisher3010.publisher3010FormValues();
        this.newPublisher.bookSet = [];
            if (this.publisher3010.isFormValid()) {
                console.log(this.newPublisher);
                this.publisherService.create(this.newPublisher)
                    .subscribe((result: {success: boolean; contentList: Publisher[]; totalElements: number}) => {
                        if (result.success) {
                            console.log('success');
                            this.openSuccessDialog('Create User', `Successfuly created publisher`, 'success');
                        } else {
                                console.log('fail');
                            this.openFailDialog('User', `Failed to create new publisher.`, 'warning');
                        }
                    });
            } else {

            }
    }

    openSuccessDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.backToList();
        console.log('The dialog was closed');
        });
    }

    openFailDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.onClickReset();
        console.log('The dialog was closed');
        });
    }

    backToList() {
        // this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/publisher/list');
    }

}
