import { Component, OnInit } from '@angular/core';
import { PublisherService } from 'src/app/shared/service/publisher.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Publisher } from 'src/app/shared/model/publisher';

@Component({
  selector: 'app-publisher3010-form-panel',
  templateUrl: './publisher3010-form-panel.component.html',
  styleUrls: ['./publisher3010-form-panel.component.scss']
})
export class Publisher3010FormPanelComponent implements OnInit {

    public publisher3010Form: FormGroup;
    public publisher3010FormControl;

    public hide = false;

    public errorMessage = 'Required Field';

    public formDisable: false;

    public newPublisher = new Publisher();

    constructor(
        private fb: FormBuilder,
        private publisherService: PublisherService
    ) {
        this.createForm();
     }

    ngOnInit(): void {
    }

    createForm() {
        this.publisher3010Form = this.fb.group({
            name: ['', [Validators.required]],
            city: ['', [Validators.required]],
            country: ['', [Validators.required]],
            email: '',
            telephone: ''
        });
        this.publisher3010FormControl = this.publisher3010Form.controls;
    }

    publisher3010FormValues() {
        this.markFormGroupTouched(this.publisher3010Form);
        return this.publisher3010Form.value;
    }

    isFormValid() {
        return this.publisher3010Form.valid;
    }

    markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
        control.markAsTouched();
        if (control.controls) {
            this.markFormGroupTouched(control);
        }
        });
    }

    onReset() {
        this.createForm();
        // console.log(this.user3010Form.hasError('notMatch'));
    }

}
