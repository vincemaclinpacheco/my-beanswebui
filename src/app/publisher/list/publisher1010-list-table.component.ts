import { Component, OnInit, NgZone } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { Publisher } from 'src/app/shared/model/publisher';
import { PublisherService } from 'src/app/shared/service/publisher.service';
import { Router } from '@angular/router';
import { DateRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/date-renderer.component';

@Component({
  selector: 'app-publisher1010-list-table',
  templateUrl: './publisher1010-list-table.component.html',
  styleUrls: ['./publisher1010-list-table.component.scss']
})
export class Publisher1010ListTableComponent implements OnInit {

    public gridOptions: GridOptions;
    public gridApi;
    public gridColumnApi;

    public totalElements: number;
    public publisherList: Publisher[] = [];
    public selectedPublisher: Publisher;

    // MatPaginator
    public pageSize = 10;
    public pageSizeOptions: number[] = [10, 20];

    constructor(
        private publisherService: PublisherService,
        private router: Router,
        private ngZone: NgZone
    ) {
        this.gridOptions = { 
            columnDefs: [
                { headerName: 'Name', field: 'name', sortable: true, width: 250 },
                { headerName: 'City', field: 'city', sortable: true, width: 130 },
                { headerName: 'Country', field: 'country', sortable: true, width: 130 },
                { headerName: 'Email', field: 'email', sortable: true, width: 250 },
                { headerName: 'Telephone', field: 'telephone', sortable: true, width: 130 }
    
             ],
             enableColResize: true,
             rowSelection: 'single',
             frameworkComponents : {
                dateRenderer: DateRendererComponent
            }
        };
     }

    ngOnInit(): void {
        this.listPublishers(0, 10);
    }

    listPublishers(pageNo: number, pageSize: number) {
        this.publisherService
            .list(pageNo, pageSize)
            .subscribe((result: {success: boolean; contentList: Publisher[]; totalElements: number; }) => {
                this.publisherList = result.contentList;
                this.totalElements = result.totalElements;
                console.log(this.publisherList);
            });
    }

    onSelectPublisher() {
        const selectedNode = this.gridApi.getSelectedNodes();
        const selectedData = selectedNode.map( node => node.data );
        this.selectedPublisher = selectedData[0];
        console.log(this.selectedPublisher);
        this.changeRoute(this.selectedPublisher.id);
   
    }

    changeRoute(id: number) {
        // this.router.navigate(['user/display/' + id]);
        this.ngZone.run(() => this.router.navigate(['publisher/display/' + id])).then();
    }

    onPageSelect(e: any) {
        if (this.publisherList.length > 0) {
            this.listPublishers(e.pageIndex, e.pageSize);
        }
    }

    refreshGrid() {
    this.gridApi.refreshCells();
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

}
