import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { AgGridCustomModule } from '../shared/ag-grid-custom/ag-grid-custom.module';
import { AuthorRoutingModule } from './author-routing.module';
import { AuthorComponent } from './author.component';
import { Author1000ListComponent } from './list/author1000-list.component';
import { Author1010ListTableComponent } from './list/author1010-list-table.component';
import { Author2000DisplayComponent } from './display/author2000-display.component';
import { Author2010DisplayDetailsComponent } from './display/author2010-display-details.component';
import { Author4000SearchComponent } from './search/author4000-search.component';
import { Author4010SearchPanelComponent } from './search/author4010-search-panel.component';
import { Author4020SearchResultsComponent } from './search/author4020-search-results.component';
import { Author3000FormComponent } from './form/author3000-form.component';
import { Author3010FormPanelComponent } from './form/author3010-form-panel.component';



@NgModule({
  declarations: [Author1000ListComponent, Author1010ListTableComponent, Author2000DisplayComponent, Author2010DisplayDetailsComponent, AuthorComponent, Author4000SearchComponent, Author4010SearchPanelComponent, Author4020SearchResultsComponent, Author3000FormComponent, Author3010FormPanelComponent],
  imports: [
    CommonModule,
    AuthorRoutingModule,
    AngularMaterialModule,
    AgGridCustomModule,
    SharedModule
  ],
  exports: [
    Author2000DisplayComponent,
    Author2010DisplayDetailsComponent,
    Author3000FormComponent,
    Author3010FormPanelComponent
  ]
})
export class AuthorModule { }
