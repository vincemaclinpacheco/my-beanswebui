import { Component, OnInit, ViewChild } from '@angular/core';
import { Author3010FormPanelComponent } from './author3010-form-panel.component';
import { AuthorService } from 'src/app/shared/service/author.service';
import { Author } from 'src/app/shared/model/author';
import { AlertModalViewComponent } from 'src/app/shared/angular-material/alert-modal/alert-modal.component';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/shared/model/user';

@Component({
  selector: 'app-author3000-form',
  templateUrl: './author3000-form.component.html',
  styleUrls: ['./author3000-form.component.scss']
})
export class Author3000FormComponent implements OnInit {

    @ViewChild(Author3010FormPanelComponent)
    private author3010: Author3010FormPanelComponent;

    public newAuthor: Author;

    constructor(
        private router: Router,
        private authorService: AuthorService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
    }

    onClickReset() {
        this.author3010.onReset();
    }

    onClickAdd() {
        this.newAuthor = new Author();
        this.newAuthor = this.author3010.author3010FormValues();
        this.newAuthor.bookSet = [];
            if (this.author3010.isFormValid()) {
                console.log(this.newAuthor);
                this.authorService.create(this.newAuthor)
                    .subscribe((result: {success: boolean; contentList: Author[]; totalElements: number}) => {
                        if (result.success) {
                            console.log('success');
                            this.openSuccessDialog('Create User', `Successfuly created author`, 'success');
                        } else {
                                console.log('fail');
                            this.openFailDialog('User', `Failed to create new author.`, 'warning');
                        }
                    });
            } else {

            }
    }

    openSuccessDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.backToList();
        console.log('The dialog was closed');
        });
    }

    openFailDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.onClickReset();
        console.log('The dialog was closed');
        });
    }

    backToList() {
        // this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/author/list');
    }

}
