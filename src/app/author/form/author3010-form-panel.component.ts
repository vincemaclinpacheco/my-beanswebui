import { Component, OnInit } from '@angular/core';
import { Author } from 'src/app/shared/model/author';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthorService } from 'src/app/shared/service/author.service';

@Component({
  selector: 'app-author3010-form-panel',
  templateUrl: './author3010-form-panel.component.html',
  styleUrls: ['./author3010-form-panel.component.scss']
})
export class Author3010FormPanelComponent implements OnInit {

    public author3010Form: FormGroup;
    public author3010FormControl;

    public hide = false;

    public errorMessage = 'Required Field';

    public formDisable: false;

    public newAuthor = new Author();

    constructor(
        private fb: FormBuilder,
        private authorService: AuthorService
    ) {
        this.createForm();
     }

    ngOnInit(): void {
    }

    createForm() {
        this.author3010Form = this.fb.group({
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            email: '',
        });
        this.author3010FormControl = this.author3010Form.controls;
    }

    author3010FormValues() {
        this.markFormGroupTouched(this.author3010Form);
        return this.author3010Form.value;
    }

    isFormValid() {
        return this.author3010Form.valid;
    }

    markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
        control.markAsTouched();
        if (control.controls) {
            this.markFormGroupTouched(control);
        }
        });
    }

    onReset() {
        this.createForm();
        // console.log(this.user3010Form.hasError('notMatch'));
    }

}
