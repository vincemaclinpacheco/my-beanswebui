import { Component, OnInit, NgZone } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { Author } from 'src/app/shared/model/author';
import { AuthorService } from 'src/app/shared/service/author.service';
import { Router } from '@angular/router';
import { DateRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/date-renderer.component';

@Component({
  selector: 'app-author1010-list-table',
  templateUrl: './author1010-list-table.component.html',
  styleUrls: ['./author1010-list-table.component.scss']
})
export class Author1010ListTableComponent implements OnInit {

    public gridOptions: GridOptions;
    public gridApi;
    public gridColumnApi;

    public totalElements: number;
    public authorList: Author[] = [];
    public selectedAuthor: Author;

    // MatPaginator
    public pageSize = 10;
    public pageSizeOptions: number[] = [10, 20];

    constructor(
        private authorService: AuthorService,
        private router: Router,
        private ngZone: NgZone
    ) {
        this.gridOptions = { 
            columnDefs: [
                { headerName: 'Author', width: 250, colId: 'authorFullName', sortable: true, valueGetter: function nameConcat(params) {
                    return params.data.firstName + ' ' + params.data.lastName;
                }},
                { headerName: 'Email', field: 'email', sortable: true, width: 250 }
    
             ],
             enableColResize: true,
             rowSelection: 'single',
             frameworkComponents : {
                dateRenderer: DateRendererComponent
            }
        };
     }

    ngOnInit(): void {
        this.listAuthors(0, 10);
    }

    listAuthors(pageNo: number, pageSize: number) {
        this.authorService
            .list(pageNo, pageSize)
            .subscribe((result: {success: boolean; contentList: Author[]; totalElements: number; }) => {
                this.authorList = result.contentList;
                this.totalElements = result.totalElements;
                console.log(this.authorList);
            });
    }

    onSelectAuthor() {
        const selectedNode = this.gridApi.getSelectedNodes();
        const selectedData = selectedNode.map( node => node.data );
        this.selectedAuthor = selectedData[0];
        console.log(this.selectedAuthor);
        this.changeRoute(this.selectedAuthor.id);
   
    }

    changeRoute(id: number) {
        // this.router.navigate(['user/display/' + id]);
        this.ngZone.run(() => this.router.navigate(['author/display/' + id])).then();
    }

    onPageSelect(e: any) {
        if (this.authorList.length > 0) {
            this.listAuthors(e.pageIndex, e.pageSize);
        }
    }

    refreshGrid() {
    this.gridApi.refreshCells();
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

}
