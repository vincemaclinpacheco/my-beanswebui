import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorComponent } from './author.component';
import { Author1000ListComponent } from './list/author1000-list.component';
import { Author2000DisplayComponent } from './display/author2000-display.component';
import { Author4000SearchComponent } from './search/author4000-search.component';
import { Author3000FormComponent } from './form/author3000-form.component';

const AUTHOR_ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: '',
        component: AuthorComponent,
        children: [
            {
                path: 'list',
                component: Author1000ListComponent
            },
            {
                path:'display/:id',
                component: Author2000DisplayComponent
            },
            {
                path:'new',
                component: Author3000FormComponent
            },
            {
                path:'search',
                component: Author4000SearchComponent
            },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(AUTHOR_ROUTES)],
  exports: [RouterModule]
})
export class AuthorRoutingModule { }
