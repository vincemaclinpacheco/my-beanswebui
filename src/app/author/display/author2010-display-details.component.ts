import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Author } from 'src/app/shared/model/author';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-author2010-display-details',
  templateUrl: './author2010-display-details.component.html',
  styleUrls: ['./author2010-display-details.component.scss']
})
export class Author2010DisplayDetailsComponent implements OnInit {

    @Input()
    authorToDisplay: Author;

    @Input()
    formDisable: boolean;

    public author2010Form: FormGroup;
    public author2010FormControl;

    public disabled = false;

    constructor(
        private fb: FormBuilder
    ) {
        this.createForm();
     }

    ngOnInit(): void {
    }

    ngOnChanges() {
        if (this.authorToDisplay) {
            this.author2010FormControl['id'].setValue(this.authorToDisplay.id);
            this.author2010FormControl['firstName'].setValue(this.authorToDisplay.firstName);
            this.author2010FormControl['lastName'].setValue(this.authorToDisplay.lastName);
            this.author2010FormControl['email'].setValue(this.authorToDisplay.email);
        }
    
        console.log(this.formDisable);
    }

    createForm() {
        this.author2010Form = this.fb.group({
            id: 0,
            firstName: '',
            lastName: '',
            email: ''
        });
        this.author2010FormControl = this.author2010Form.controls;
    }

    author2010FormValues() {
        this.markFormGroupTouched(this.author2010Form);
        return this.author2010Form.value;
    }
    
    isFormValid() {
        return this.author2010Form.valid;
    }
    
    markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
            control.markAsTouched();
                if (control.controls) {
                    this.markFormGroupTouched(control);
                }
            });
    }

}
