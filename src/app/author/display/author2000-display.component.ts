import { Component, OnInit, ViewChild } from '@angular/core';
import { Author2010DisplayDetailsComponent } from './author2010-display-details.component';
import { Author } from 'src/app/shared/model/author';
import { AuthorService } from 'src/app/shared/service/author.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AlertModalViewComponent } from 'src/app/shared/angular-material/alert-modal/alert-modal.component';

@Component({
  selector: 'app-author2000-display',
  templateUrl: './author2000-display.component.html',
  styleUrls: ['./author2000-display.component.scss']
})
export class Author2000DisplayComponent implements OnInit {

    @ViewChild(Author2010DisplayDetailsComponent)
    private author2010: Author2010DisplayDetailsComponent;

    public authorToDisplay: Author;
    public authorToUpdate: Author;
    public formDisable = true;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private authorService: AuthorService,
        public dialog: MatDialog
    ) { }

    ngOnInit(): void {
        const id = this.activatedRoute.snapshot.params['id'];
        this.getAuthor(id);
    }

    getAuthor(id: number) {
        this.authorService
        .get(id)
        .subscribe(result => {
            this.authorToDisplay = result;
            console.log(this.authorToDisplay);
        });
    }

    onClickEdit() {
        this.formDisable = false;
    }

    onClickReset() {
        this.formDisable = true;
    }

    onClickSave() {
        this.authorToUpdate = new Author();
        this.authorToUpdate = this.author2010.author2010FormValues();
        this.authorToUpdate.bookSet = this.authorToDisplay.bookSet;
        console.log(this.authorToUpdate);
        if (this.author2010.isFormValid()) {
            this.authorService
            .update(this.authorToUpdate)
            .subscribe((result: {success: boolean; contentList: Author[]; totalElements: number; }) => {
                if (result.success) {
                    // console.log('success');
                    this.authorToUpdate = new Author();
                    this.authorToUpdate = result.contentList[0];
                    this.formDisable = true;
                    this.openSuccessDialog('Edit Author', `Successfuly updated author`, 'success');
                    } else {
                        // console.log('fail');
                        // this.modalHeaderText = this.messageService.getValue('userWarning');
                        // this.modalBodyText = 'Update Failed';
                        // this.modalType = 'warning';
                        // this.alertModal.openDialog(this.modalHeaderText, this.modalBodyText, this.modalType);
                        this.openFailDialog('Edit Author', `Failed to update author.`, 'warning');
                    }
                });
        }
    }

    openSuccessDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
        this.backToList();
        console.log('The dialog was closed');
        });
    }

    openFailDialog(headerText: string, bodyText: string, type: string) {
        const dialogRef = this.dialog.open(AlertModalViewComponent, {
        width: '500px',
        height: '200px',
        data: {header: headerText, body: bodyText, alertType: type},
    });
        dialogRef.afterClosed().subscribe(result => {
            const id = this.activatedRoute.snapshot.params['id'];
            this.getAuthor(id);
        });
    }

    backToList() {
        // this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/author/list');
    }

}
