import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-author4010-search-panel',
  templateUrl: './author4010-search-panel.component.html',
  styleUrls: ['./author4010-search-panel.component.scss']
})
export class Author4010SearchPanelComponent implements OnInit {

    public author4010Form: FormGroup;
    public author4010FormControl;

    constructor(
        private fb: FormBuilder
    ) {
        this.createForm();
     }

    ngOnInit(): void {
    }

    createForm() {
        this.author4010Form = this.fb.group({
            id: 0,
            firstName: '',
            lastName: ''
        });
        this.author4010FormControl = this.author4010Form.controls;
    }

    author4010FormValues() {
        return this.author4010Form.value;
    }

    onReset() {
        this.createForm();
    }

}
