import { Component, OnInit, ViewChild } from '@angular/core';
import { Author4010SearchPanelComponent } from './author4010-search-panel.component';
import { AuthorService } from 'src/app/shared/service/author.service';
import { Author } from 'src/app/shared/model/author';
import { QueryParam, Operator } from 'src/app/shared/model/query';
import { Router } from '@angular/router';

@Component({
  selector: 'app-author4000-search',
  templateUrl: './author4000-search.component.html',
  styleUrls: ['./author4000-search.component.scss']
})
export class Author4000SearchComponent implements OnInit {

    @ViewChild(Author4010SearchPanelComponent)
    private author4010Form: Author4010SearchPanelComponent;

    public queryParams: QueryParam[] = [];
    public searchedAuthorList: Author[] = [];
    public pageSize = 25;
    public totalElements: number;

    public authorForm: any;

    constructor(
        private authorService: AuthorService,
        private router: Router
    ) { }

    ngOnInit(): void {
    }

    onClickReset() {
        this.author4010Form.onReset();
    }

    onClickSearch() {
        this.queryParams = [];
        this.searchedAuthorList = [];

        this.authorForm = this.author4010Form.author4010FormValues();
        /* Book Form Parameter */
        if (this.authorForm.firstName !== '') {
            let firstNameParam: QueryParam;
            this.authorForm.firstName = this.authorForm.firstName.trim();
            this.authorForm.firstName = `*${this.authorForm.firstName}*`
  
            firstNameParam = new QueryParam('firstName', Operator.LIKE, this.authorForm.firstName);

            this.queryParams.push(firstNameParam);
        }

        if (this.authorForm.lastName !== '') {
            let lastNameParam: QueryParam;
            this.authorForm.lastName = this.authorForm.lastName.trim();
            this.authorForm.lastName = `*${this.authorForm.lastName}*`
  
            lastNameParam = new QueryParam('lastName', Operator.LIKE, this.authorForm.lastName);

            this.queryParams.push(lastNameParam);
        }

        // if (this.authorForm.lastName !== '') {
        //     const lastNameParam = new QueryParam('lastName', Operator.LIKE, this.authorForm.lastName);
        //     this.queryParams.push(lastNameParam);
        // }

        // if (this.authorForm.role !== '') {
        //     const roleParam = new QueryParam('role', Operator.EQIC, this.authorForm.role);
        //     this.queryParams.push(roleParam);
        // }

        // if (this.authorForm.email !== '') {
        //     const emailParam = new QueryParam('email', Operator.EQIC, this.authorForm.email);
        //     this.queryParams.push(emailParam);
        // }

        
        // if (this.authorForm.lastLoginStartDate !== '') {
        //     const saleStartDateParam = new QueryParam('lastLoginDate', Operator.GTE, this.authorForm.lastLoginStartDate);
        //     this.queryParams.push(saleStartDateParam);
        // }

        // if (this.authorForm.lastLoginEndDate !== '') {
        //     const saleEndDateParam = new QueryParam('lastLoginDate', Operator.LTE, this.authorForm.lastLoginEndDate);
        //     this.queryParams.push(saleEndDateParam);
        // }

        this.searchAuthor(0, this.pageSize);

    }

    searchAuthor(page: number, pageSize: number) {
        this.authorService
        .search(this.queryParams, page, pageSize)
        .subscribe((result: any) => {
            this.searchedAuthorList = result.contentList;
            this.totalElements = result.totalElements;
            console.log(this.searchedAuthorList);
        });
    }

    checkAuthorResultLength() {
        if (this.searchedAuthorList.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    onClickBack() {
        this.searchedAuthorList = [];
    }

}
