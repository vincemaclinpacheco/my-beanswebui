import { Component, OnInit, Input, NgZone } from '@angular/core';
import { Author } from 'src/app/shared/model/author';
import { AuthorService } from 'src/app/shared/service/author.service';
import { GridOptions } from 'ag-grid-community';
import { Router } from '@angular/router';
import { DateRendererComponent } from 'src/app/shared/ag-grid-custom/renderer/date-renderer.component';

@Component({
  selector: 'app-author4020-search-results',
  templateUrl: './author4020-search-results.component.html',
  styleUrls: ['./author4020-search-results.component.scss']
})
export class Author4020SearchResultsComponent implements OnInit {

    @Input()
    searchedAuthorList: Author[] = [];

    @Input()
    totalElements: number;

    public gridOptions: GridOptions;
    public gridApi;
    public gridColumnApi;

    public selectedAuthor: Author;


    constructor(
        private authorService: AuthorService,
        private router: Router,
        private ngZone: NgZone
    ) {
        this.gridOptions = { 
            columnDefs: [
                { headerName: 'Author', width: 250, colId: 'authorFullName', sortable: true, valueGetter: function nameConcat(params) {
                    return params.data.firstName + ' ' + params.data.lastName;
                }},
                { headerName: 'Email', field: 'email', sortable: true, width: 250 }
    
             ],
             enableColResize: true,
             rowSelection: 'single',
             frameworkComponents : {
                dateRenderer: DateRendererComponent
            }
        };
     }

    ngOnInit(): void {
    }

    onSelectAuthor() {
        const selectedNode = this.gridApi.getSelectedNodes();
        const selectedData = selectedNode.map( node => node.data );
        this.selectedAuthor = selectedData[0];
        this.changeRoute(this.selectedAuthor.id);
   
    }

    changeRoute(id: number) {
        // this.router.navigate(['user/display/' + id]);
        this.ngZone.run(() => this.router.navigate(['author/display/' + id])).then();
    }

    refreshGrid() {
        this.gridApi.refreshCells();
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

}
