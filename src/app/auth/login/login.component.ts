import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SessionStorage, SessionStorageService } from 'ngx-webstorage';
import * as crypto from 'crypto-js';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/shared/model/user';
import { LoginService } from 'src/app/shared/service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    // required for encryption
    public CryptoJS = require('crypto-js');

    public user = new User();
    public loginForm: FormGroup;

    public hide = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    private sessionService: SessionStorageService,
  ) {
      this.createForm();
   }

  ngOnInit(): void {
  }

  createForm() {
    this.loginForm = this.fb.group({
        userId: ['', [Validators.required]],
        password: ['', [Validators.required]]
    });
  }

  login() {
      console.log(this.loginForm)
      this.user = new User();
      this.user.userId = this.loginForm.get('userId').value;
      this.user.password = this.loginForm.get('password').value;

      if (this.loginForm.valid) {
        this.loginService.loginUser(this.user)
            .subscribe((result: {
                success: boolean; contentList: any; totalElements: number; errorMessage: any
            }) => {
                if (result.success) {
                    // encrypt UserSession object then store it in Session Storage
                    const ciphertext = this.CryptoJS.AES.encrypt(JSON.stringify(result.contentList), 'b3@ns').toString();
                    this.sessionService.store('beansData', ciphertext);
                    // this.sessionService.store('beansData', result.contentList[0]);
                    this.router.navigateByUrl('/book');
                
                } else {
                    if (result.errorMessage === 'expired' || result.errorMessage === 'temporary') {
                        this.loginService.userToLogin = result.contentList[0][0];
                        this.loginService.userToLogin.password = result.contentList[0][1];
                        console.log(this.loginService.userToLogin);
                        console.log('expired');
                        this.router.navigateByUrl('auth/change_password');
                    } else {
                        // this.errorMessage = result.errorMessage.replace(';', '.');
                        // this.loginSuccess = false;
                        // this.displayError = true;
                    }
                }
            })
      }
  }

}
