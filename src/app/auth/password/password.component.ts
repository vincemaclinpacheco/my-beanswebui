import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormGroupDirective, NgForm, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { User } from 'src/app/shared/model/user';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/shared/service/login.service';
import { ErrorStateMatcher } from '@angular/material/core';


/** Error when the parent is invalid */
class CrossFieldErrorMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      return control.dirty && form.invalid;
    }
}

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {

public passwordForm: FormGroup;

public formSubmitAttempt: boolean;
public changePasswordAttempt = false;

public tempUser = new User();

public bool1 = true;
public hide = false;

errorMatcher = new CrossFieldErrorMatcher();

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private loginService: LoginService
  ) { }

  ngOnInit(): void {
    this.initTempUser();
    this.createForm();
  }

  initTempUser() {
        this.tempUser = this.loginService.userToLogin;
    }

    createForm() {
        // initialize form group
        this.passwordForm = this.fb.group({
            oldPassword: ['',
                [Validators.required,
                    this.oldPasswordValidator(this.tempUser.password)
                ]
            ],
            newPassword: ['',
                [Validators.required,
                this.minLengthValidator,
                this.alphaNumericValidator,
                this.noRepeatingValidator,
                this.checkUsernameValidator(this.tempUser.userId)
                ]
            ],
            confirmPassword: ['', [Validators.required]]
        }, { validator: this.checkMatchValidator });
    }

    minLengthValidator(control: FormControl) {
        if (control.value.length >= 8) {
            return null;
            } else {
            return {
                minLength: true
            };
        }
    }

    alphaNumericValidator(control: FormControl) {
        const alphaNumericRegexp = /^(?=(.*[a-zA-Z].*){1,})(?=.*\d.*)[a-zA-Z0-9\S]{0,}$/;
        if (alphaNumericRegexp.test(control.value)) {
            return null;
            } else {
            return {
                invalidPattern: true
            };
        }
    }

    // tslint:disable-next-line:ban-types
    oldPasswordValidator(password: String): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (control.value !== password) {
            return { incorrectPassword: true };
        }
        return null;
        };
    }

    // tslint:disable-next-line:ban-types
    checkUsernameValidator(userName: String): ValidatorFn {
        return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (control.value.includes(userName) || control.value === '') {
            return { containsUsername: true };
        }
        return null;
        };
    }

    noRepeatingValidator(control: FormControl) {
        const noRepeatingRegexp = /^(?!.*(\w)\1{2,}).+$/;
        if (noRepeatingRegexp.test(control.value)) {
            return null;
            } else {
            return {
                repeatingChar: true
            };
        }
    }

    checkMatchValidator(group: FormGroup) {
        const newPass = group.controls.newPassword.value;
        const confirmPass = group.controls.confirmPassword.value;

        return newPass === confirmPass ? null : {notMatch: true};
    }

    onNewChange(abc: string) {
        this.bool1 = this.passwordForm.get('newPassword').hasError('minLength');
        // console.log(abc);
        // console.log(this.bool1);
    }

    changePassword() {
        this.formSubmitAttempt = true;
        this.changePasswordAttempt = true;
        if (this.passwordForm.valid) {
            console.log('password valid');
            this.tempUser.newPassword = this.passwordForm.get('newPassword').value;
            this.loginService.updatePassword(this.tempUser)
                .subscribe((result: { success: boolean; contentList: any; totalElements: number }) => {
                    this.changePasswordAttempt = false;
                    if (result.success) {
                        // this.openSuccessDialog('Change Password', 'Password changed', 'info');
                        console.log('password changed');
                        this.backToLogin();
                    } else {
                        // this.openFailedDialog('Change Password', 'Password is invalid or already been used.', 'alert');
                        console.log('password not updated');
                    }
                });
            } else {
            console.log('password not valid');
        }
    console.log(this.passwordForm.get('oldPassword').hasError('incorrectPassword'));
    console.log(this.passwordForm.get('newPassword').hasError('minLength'));
    console.log(this.passwordForm.get('newPassword').hasError('invalidPattern'));
    console.log(this.passwordForm.get('newPassword').hasError('containsUsername'));
    console.log(this.passwordForm.get('newPassword').hasError('repeatingChar'));
    console.log(this.passwordForm.hasError('notMatch'));
    }

    backToLogin() {
        // this.bool1 = !this.bool1;
        // console.log(this.bool1);
        this.loginService.clearUserToLogin();
        this.router.navigateByUrl('/auth/login');
    }

}
