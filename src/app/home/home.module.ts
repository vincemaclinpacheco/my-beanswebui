import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { NotFoundComponent } from './not-found.component';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { TestComponent } from './test.component';

@NgModule({
  declarations: [HomeComponent, NotFoundComponent, TestComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    AngularMaterialModule
  ],
  exports: [
    HomeComponent,
    NotFoundComponent,
    AngularMaterialModule
  ]
})
export class HomeModule { }
