import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthService } from '../shared/service/auth.service';
import { SidenavService } from '../shared/service/sidenav.service';
import { MatSidenav } from '@angular/material/sidenav';
import { User } from '../shared/model/user';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

@ViewChild('sidenav') public sidenav: MatSidenav;

    opened: boolean;
    user: User;
    public currentBaseRoute = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sidenavService: SidenavService,
    private authService: AuthService,
    private sessionService: SessionStorageService
  ) {
      router.events.subscribe((val) => {
          if (val instanceof NavigationEnd) {
              const currentRouteArr = val.url.split('/');
              this.currentBaseRoute = currentRouteArr[1];
              console.log(this.currentBaseRoute);

          }
          this.sidenavService.setSidenav(this.sidenav);
      
      })
   }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  ngAfterViewInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
  }

  toggleSidenav() {
      this.sidenavService.toggle();
  }

  test() {
    console.log(this.authService.getSession());
    console.log(this.isAuthenticated());
    this.router.navigateByUrl('/book');
  }

  onClickLogout() {
    this.sessionService.clear('beansData');
    this.router.navigateByUrl('/auth/login');
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

}
