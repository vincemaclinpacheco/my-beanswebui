import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found.component';
import { TestComponent } from './test.component';
import { AuthGuardService as AuthGuard } from '../shared/guard/auth-guard.service';
import { RoleGuardService as RoleGuard } from '../shared/guard/role-guard.service';

const APP_ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'auth',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: 'user',
        loadChildren: () => import('../user/user.module').then(m => m.UserModule),
        canActivate: [RoleGuard],
        data: {
            expectedRole: 'admin'
        }
    },
    {
        path: 'book',
        loadChildren: () => import('../book/book.module').then(m => m.BookModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'author',
        loadChildren: () => import('../author/author.module').then(m => m.AuthorModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'publisher',
        loadChildren: () => import('../publisher/publisher.module').then(m => m.PublisherModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'test',
        component: TestComponent
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];


@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES, {useHash: true})
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
